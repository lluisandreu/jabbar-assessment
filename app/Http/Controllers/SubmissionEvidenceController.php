<?php

namespace App\Http\Controllers;

use App\SubmissionEvidence;
use Illuminate\Http\Request;

class SubmissionEvidenceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubmissionEvidence  $submissionEvidence
     * @return \Illuminate\Http\Response
     */
    public function show(SubmissionEvidence $submissionEvidence)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubmissionEvidence  $submissionEvidence
     * @return \Illuminate\Http\Response
     */
    public function edit(SubmissionEvidence $submissionEvidence)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubmissionEvidence  $submissionEvidence
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubmissionEvidence $submissionEvidence)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubmissionEvidence  $submissionEvidence
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubmissionEvidence $submissionEvidence)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\EvidenceArea;
use App\Evidence;
use App\SubmissionEvidence;

class FormController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function form($id) {
    	$profile = Profile::find($id);
        $competences = $profile->position->competences;
    	$evidences = Evidence::all();
    	$areas = EvidenceArea::all();
    	$submissions = $profile->submission->submissions;

    	return view('forms.form', ['profile' => $profile, 'areas' => $areas, 'submissions' => $submissions, 'evidences' => $evidences, 'competences' => $competences]);
    }

    public function post_submission(Request $request, $id) {

    	$submission = Profile::find($id)->submission;
    	$value = SubmissionEvidence::updateOrCreate(
    		[
                'submission_id' => $submission->id, 
                'valuable_id' => $request->id,
                'valuable_type' => $request->type == "evidence" ? "App\Evidence" : "App\DictionaryCompetence", 
                'indicator_id' => $request->indicator_id,
                'type' => $request->type
            ],
    		['result' => $request->value]
    	);
    	if($value) {
    		return response()->json('success', 200);
    	} else {
    		return response()->json(['error' => $request->all()], 500);
    	}
    }
}

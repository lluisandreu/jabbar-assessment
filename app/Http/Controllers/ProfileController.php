<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Importer;

class ProfileController extends Controller
{
    

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $profiles = Profile::paginate(15);
        return view('profiles.index', ['profiles' => $profiles]);
    }

    public function index_json() {

        $object = collect();
        foreach (Profile::all() as $key => $profile) {
            $item = collect();
            $item->put('id', $profile->id);
            $item->put('picture',  asset('public/profiles/'.$profile->picture));
            $item->put('name', $profile->name);
            $item->put('first_surname', $profile->first_surname);
            $item->put('second_surname', $profile->second_surname);
            $item->put('email', $profile->email);
            $item->put('company', optional($profile->company)->name);
            $item->put('position', optional($profile->position)->name);
            $item->put('project', optional($profile->project)->name);

            if($profile->questionaryStatus() == 0) {
                $item->put('formstatus', '<i class="fa fa-tick"></i>');
            } else {
                $item->put('formstatus', '<span class="badge">Faltan ' . $profile->questionaryStatus() . ' por completar</span>');
            }
            $object->push($item);
            
        }
        $data = collect($object);
        return $data->toJson();
    }

    public function competences_json(Request $request) {
        $profile = Profile::find($request->profile_id);
        $skill = \App\Skill::where('indicator_id', $request->indicator_id)->first();
        if($skill) {
            $competences = $profile->position->competences->where('skill_id', $skill->id);
            $competences->map(function ($item) use($request) {
                return [
                'id' => $item['id'],
                'body' => $item['description'],
                'indicator_id' => $request->indicator,
                'valuations' => serialize($item->levels)
                ];
            });
            if(count($competences)) {
                return $competences->toJson();
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('profiles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'first_surname' => 'required',
            'birthdate' => 'required|date',
            'email' => 'required|unique:profiles,email|email',
            'cv' => 'file|mimes:pdf,docx',
            'picture' => 'image|mimes:jpeg,jpg',
            'project' => 'required',
            'company' => 'required',
            'position' => 'required',
            'consultor' => 'required',
            'gender' => 'required',
        ]);


        $profile = new Profile();
        $profile->user_id = $request->input('consultor');
        $profile->gender = $request->input('gender');
        $profile->company_id = $request->input('company');
        $profile->name = $request->input('name');
        $profile->first_surname = $request->input('first_surname');
        $profile->second_surname = $request->input('second_surname');
        $profile->birthdate = $request->input('birthdate');
        $profile->email = $request->input('email');
        $profile->telephone = $request->input('telephone');
        $profile->province = $request->input('province');
        $profile->city = $request->input('city');
        $profile->company_id = $request->input('company');
        $profile->position_id = $request->input('position');
        $profile->project_id = $request->input('project');
        $profile->function = $request->input('function');
        $profile->evaluation_date = $request->input('evaluation_date');
        $profile->linkedin = $request->input('linkedin');
        $profile->facebook = $request->input('facebook');
        $profile->twitter = $request->input('twitter');

        if($request->hasFile('picture')) {
            $request->file('picture')->store('public/profiles');
            $file_name = $request->file('picture')->hashName();
            $profile->picture = $file_name;
        }
        if($request->hasFile('cv')) {
            $request->file('cv')->store('public/cv');
            $file_name = $request->file('cv')->hashName();
            $profile->cv = $file_name;
        }

        $profile->save();

        $profile->submission()->create(['status' => 0]);

        return redirect('profiles')->with('status', 'Perfil creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit(Profile $profile)
    {
        return view('profiles.create', ['profile' => $profile]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'first_surname' => 'required',
            'birthdate' => 'required|date',
            'email' => 'required|email',
            'cv' => 'file|mimes:pdf,docx',
            'picture' => 'image|mimes:jpeg,jpg',
            'project' => 'required',
            'company' => 'required',
            'position' => 'required',
            'consultor' => 'required',
            'gender' => 'required',
        ]);

        $profile->user_id = $request->input('consultor');
        $profile->name = $request->input('name');
        $profile->gender = $request->input('gender');
        $profile->first_surname = $request->input('first_surname');
        $profile->second_surname = $request->input('second_surname');
        $profile->birthdate = $request->input('birthdate');
        $profile->email = $request->input('email');
        $profile->telephone = $request->input('telephone');
        $profile->province = $request->input('province');
        $profile->city = $request->input('city');
        $profile->company_id = $request->input('company');
        $profile->position_id = $request->input('position');
        $profile->project_id = $request->input('project');
        $profile->function = $request->input('function');
        $profile->evaluation_date = $request->input('evaluation_date');
        $profile->linkedin = $request->input('linkedin');
        $profile->facebook = $request->input('facebook');
        $profile->twitter = $request->input('twitter');

        if($request->hasFile('picture')) {
            $request->file('picture')->store('public/profiles');
            $file_name = $request->file('picture')->hashName();
            $profile->picture = $file_name;
        }
        if($request->hasFile('cv')) {
            $request->file('cv')->store('public/cv');
            $file_name = $request->file('cv')->hashName();
            $profile->cv = $file_name;
        }

        $profile->save();

        return redirect('profiles/' . $profile->id . '/edit')->with('status', 'Perfil actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        $profile->delete();

        return redirect('profiles')->with('status', 'Perfil eliminado correctamente');
    }

    public function import(Request $request) {

        $validatedData = $request->validate([
            'company' => 'required',
            'project' => 'required',
            'file' => 'required|file|mimes:xlsx,xls|max:5000'
        ]);


        if($request->hasFile('file')){
            $path = $request->file('file')->getRealPath();
            $excel = Importer::make('Excel');
            $excel->load($path);

            $rows =  $excel->getCollection();
            // Si hay almenos una columna en el excel
            if(count($rows)) {
                $request->session()->put('rows', $excel->getCollection());
                return response()->json($rows, 200);
            } else {
                return response()->json('No rows', 422);
            }
        }
    }

    public function verify(Request $request) {

        if ($request->session()->has('rows')) {
            $rows = $request->session()->get('rows');
            $errors = collect();

            if($request->has('mapping')) {
                $mapping = json_decode($request->input('mapping'));
                $rowStart = $request->input('rowStart');
                for ($i=$rowStart; $i < count($rows); $i++) { 
                    // Validate fields that are not strings

                    // Validate date
                    if(isset($rows[$i][$mapping->birthdate]->date)) {
                        if(!date_create($rows[$i][$mapping->birthdate]->date)) {
                            $errors->push('La fecha de nacimiento de la fila ' . ($i + 1) . ' no es válida. ' . $rows[$i][$mapping->birthdate]->date);
                        }
                    } else {
                       $errors->push('El formato de la fecha de nacimiento de la fila ' . ($i + 1) . ' no es válida. ' . $rows[$i][$mapping->birthdate]);
                    }

                    // Validate date
                    if($mapping->evaluation_date != 99) {
                        if(isset($rows[$i][$mapping->birthdate]->date)) {
                            if(!date_create($rows[$i][$mapping->evaluation_date]->date)) {
                                $errors->push('La fecha de evaluación de la fila ' . ($i + 1) . ' no es válida. ' . $rows[$i][$mapping->evaluation_date]->date);
                            }
                        } else {
                            $errors->push('El formato de la fecha de evaluación de la fila ' . $i . ' no es válida. ' . $rows[$i][$mapping->evaluation_date]);
                        }
                    }
                    
                    //Validate province
                    if(is_null(\App\Province::where('name', 'LIKE', '%' . $rows[$i][$mapping->province] . '%')->first())){
                        $errors->push('No hemos encontrado ninguna provincia con el nombre de "' . $rows[$i][$mapping->province] . '" en la fila ' . ($i + 1));    
                    }

                    //Validate position in company
                    $company = \App\Company::find($request->input('company'));
                    if(is_null(\App\CompanyPosition::where('company_id', $company->id)->where('name', 'LIKE', '%' . $rows[$i][$mapping->position] . '%')->first())){
                        $errors->push('No hemos encontrado ninguna posición con el nombre de "' . $rows[$i][$mapping->position] . '" en la empresa ' . $company->name . ' en la fila ' . ($i + 1));
                    }
                }

                if(count($errors)) {
                    return response()->json($errors, 400);
                } else {
                    return response()->json('valid', 200);
                }
            }
        }
    }

    public function store_import(Request $request) {
        if ($request->session()->has('rows')) {
            $rows = $request->session()->get('rows');

            if($request->has('mapping')) {
                $mapping = json_decode($request->input('mapping'));
                $rowStart = $request->input('rowStart');

                for ($i=$rowStart; $i < count($rows); $i++) {

                    $birthdate = \Carbon\Carbon::parse($rows[$i][$mapping->birthdate]->date);
                    $province = \App\Province::where('name', 'LIKE', '%' . $rows[$i][$mapping->province] . '%')->first();
                    $position = \App\CompanyPosition::where('company_id', $request->input('company'))->where('name', 'LIKE', '%' . $rows[$i][$mapping->position] . '%')->first();
                    if($mapping->evaluation_date == 99) {
                        $evdate = \Carbon\Carbon::now();
                    } else {
                        $evdate = \Carbon\Carbon::parse($rows[$i][$mapping->evaluation_date]->date);
                    }

                    $row = [
                        'name' => $rows[$i][$mapping->name],
                        'first_surname' => $rows[$i][$mapping->first_surname],
                        'second_surname' => $rows[$i][$mapping->second_surname],
                        'birthdate' => $birthdate,
                        'telephone' => $rows[$i][$mapping->telephone],
                        'email' => $rows[$i][$mapping->email],
                        'province' => $province->id,
                        'city' => $rows[$i][$mapping->city],
                        'company_id' => $request->input('company'),
                        'project_id' => $request->input('project'),
                        'position_id' => $position->id,
                        'function' => $rows[$i][$mapping->function],
                        'evaluation_date' => $evdate,
                        'linkedin' => $mapping->linkedin == '' ? NULL : $rows[$i][$mapping->linkedin],
                        'facebook' => $mapping->facebook == '' ? NULL : $rows[$i][$mapping->facebook],
                        'twitter' => $mapping->twitter == '' ? NULL : $rows[$i][$mapping->twitter],
                    ];

                    $profile = Profile::create($row);
                    $profile->submission()->create(['status' => 0]);
                }

                return response()->json('imported', 200);
            }
        }
    }

}

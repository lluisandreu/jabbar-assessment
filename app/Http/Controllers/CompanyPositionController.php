<?php

namespace App\Http\Controllers;

use App\CompanyPosition;
use App\CompanyArea;
use App\Company;
use Illuminate\Http\Request;

class CompanyPositionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $company = Company::find($id);
        $areas = CompanyArea::all();
        return view('companies.positions', ['company' => $company, 'areas' => $areas]);
    }

    public function index_json(Request $request) {

        $company = Company::find($request->id);
        return $company->positions->toJson();
    }

    public function position_json(Request $request) {
        

        $position = CompanyPosition::find($request->id);
        $data = collect($position);
        $data->put('competences', $position->competences);
        $data->put('dictionary', $position->competences->first()->dictionary_id);
        return $data->toJson();
    }

    public function position_post(Request $request, $id) {

        $request->validate([
            'name' => 'required',
        ]);

        if($request->has('id')) {
            $position = CompanyPosition::find($request->id);
        } else {
            $position = new CompanyPosition();
        }

        $position->company_id = $id;
        $position->company_area_id = $request->input('area');
        $position->name = $request->input('name');
        $position->save();

        if($request->has('competences')) {
            $position->competences()->detach();
            $competences = json_decode($request->input('competences'));
            foreach ($competences as $key => $value) {
                $position->competences()->attach($position->id,[
                    'dictionary_competence_id' => $value->competence, 
                    'competence_level_id' => $value->level
                ]);
            }
        }

        return response()->json('ok', 200);

        // if($request->has('competences')) {
        //     $position->competences()->detach();
        //     $position->competences()->attach($request->input('competences'));
        // }

    }

    public function position_sort(Request $request) {
        if($request->has('data')) {
            $positions = $request->input('data');
            foreach($positions as $position) {
                $pos = CompanyPosition::find($position['id']);
                $pos->parent_id = 0;
                if(isset($position['parent_id'])) {
                    $pos->parent_id = $position['parent_id'];
                }
                $pos->save();
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompanyPosition  $companyPosition
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyPosition $companyPosition)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompanyPosition  $companyPosition
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $pos = CompanyPosition::find($id);
        $pos->name = $request->input('name');
        $pos->company_area_id = $request->input('area');
        $pos->save();

        $pos->competences()->detach();
        if($request->has('competences')) {
            $pos->competences()->attach($request->input('competences'));
        }

        return back()->with('status', 'Puesto ' . $pos->name . ' editado correctamente');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompanyPosition  $companyPosition
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanyPosition $companyPosition)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompanyPosition  $companyPosition
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pos = CompanyPosition::find($id);
        $pos->delete();
        return back()->with('status', 'Puesto ' . $pos->name . ' eliminado correctamente');
    }   
}

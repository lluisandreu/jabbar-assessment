<?php

namespace App\Http\Controllers;

use App\CompanyArea;
use Illuminate\Http\Request;

class CompanyAreaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $areas = CompanyArea::paginate(30);
        return view('areas.index', ['areas' => $areas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('areas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required'
        ]);

        $area = new CompanyArea();
        $area->name = $request->input('name');
        $area->save();

        return redirect('areas')->with('status', 'Área ' . $area->name . ' creada correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompanyArea  $companyArea
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyArea $companyArea)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompanyArea  $companyArea
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $area = CompanyArea::find($id);
        return view('areas.create', ['area' => $area]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompanyArea  $companyArea
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required'
        ]);

        $area = CompanyArea::find($id);
        $area->name = $request->input('name');
        $area->save();

        return redirect('areas')->with('status', 'Área ' . $area->name . ' actualizada correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompanyArea  $companyArea
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $area = CompanyArea::find($id);
        $area->delete();
        return redirect('areas')->with('status', 'Área ' . $area->name . ' eliminada correctamente');
    }
}

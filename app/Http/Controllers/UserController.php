<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(15);
        return view('users.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $newUser = new User();
        $newUser->name = $request->input('name');
        $newUser->email = $request->input('email');

        if($request->input('status')) {
            $newUser->status = $request->input('status');
        } else {
            $newUser->status = '0';
        }
        
        $newUser->company_id = $request->input('company_id');
        $newUser->password = Hash::make($request->input('password'));

        if($request->hasFile('avatar')) {
            $request->file('avatar')->store('public/avatar');
            $file_name = $request->file('avatar')->hashName();
            $user->avatar = $file_name;
        }

        $newUser->save();

        if($request->input('roles')) {
            $newUser->roles()->attach($request->input('roles'));
        }

        return redirect('users/' . $newUser->id . '/edit')->with('status', 'Nuevo usuario ' . $newUser->name . ' creado');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('users.create', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            //'email' => 'required|string|email|max:255|unique:users',
            'password' => 'string|min:6|nullable|confirmed',
        ]);

        $user->name = $request->input('name');
        //$user->email = $request->input('email');
        if($request->input('status')) {
            $user->status = $request->input('status');
        } else {
            $user->status = '0';
        }
        $user->company_id = $request->input('company_id');

        if($request->input('password')) {
            $user->password = Hash::make($request->input('password'));
        }
        
        if($request->hasFile('avatar')) {
            $request->file('avatar')->store('public/avatar');
            $file_name = $request->file('avatar')->hashName();
            $user->avatar = $file_name;
        }

        $user->save();

        if($request->input('roles')) {
            if(!$user->hasRole($request->input('roles'))) {
                $user->detachRoles($user->roles);
                $user->roles()->attach($request->input('roles'));
            }
            
        }

        return redirect('users/' . $user->id . '/edit')->with('status', 'Usuario ' . $user->name . ' actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect('users')->with('status', 'Usuario eliminado correctamente');
    }
}

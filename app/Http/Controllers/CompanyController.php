<?php

namespace App\Http\Controllers;

use App\Company;
use App\CompanyPosition;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::paginate(15);
        return view('companies.index', ['companies' => $companies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|unique:companies|max:255',
            'logo' => 'file',
            'size' => 'numeric'
        ]);

        $company = new Company();
        $company->name = $request->input('name');
        $company->size = $request->input('size');
        $company->company_sector_id = $request->input('sector');

        if($request->hasFile('logo')) {
            $request->file('logo')->store('public/logos');
            $file_name = $request->file('logo')->hashName();
            $company->logo = $file_name;
        }

        $company->save();

        return redirect('companies')->with('status', 'Empresa ' . $company->name . ' creada satisfactoriamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('companies.create', ['company' => $company]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
        ]);

        $company->name = $request->input('name');
        $company->size = $request->input('size');
        $company->company_sector_id = $request->input('sector');

        if($request->hasFile('logo')) {
            $request->file('logo')->store('public/logos');
            $file_name = $request->file('logo')->hashName();
            $company->logo = $file_name;
        }
        
        $company->save();

        return redirect('companies')->with('status', 'Empresa ' . $company->name . ' actualizada satisfactoriamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $company->delete();

        return redirect('companies')->with('status', 'Empresa eliminada correctamente');
    }
}

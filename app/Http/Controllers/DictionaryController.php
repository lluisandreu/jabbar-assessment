<?php

namespace App\Http\Controllers;

use App\Dictionary;
use App\DictionaryCompetence;
use App\CompetenceLevel;
use Illuminate\Http\Request;

class DictionaryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dictionaries = Dictionary::all();
        return view('dictionaries.index', ['dictionaries' => $dictionaries]);
    }

    public function index_json(Request $request) {

        $dictionaries = Dictionary::where('company_id', $request->id)->get();
        return $dictionaries->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('dictionaries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'companies' => 'required'
        ]);

        $dict = new Dictionary();
        $dict->name = $request->input('name');
        $dict->company_id = $request->input('companies');
        $dict->save();

        return redirect('dictionaries')->with('status', 'Diccionario ' . $dict->name . ' creado satisfactoriamente');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dictionary  $dictionary
     * @return \Illuminate\Http\Response
     */
    public function show(Dictionary $dictionary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dictionary  $dictionary
     * @return \Illuminate\Http\Response
     */
    public function edit(Dictionary $dictionary)
    {
        return view('dictionaries.create', ['dictionary' => $dictionary]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dictionary  $dictionary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dictionary $dictionary)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'companies' => 'required'
        ]);

        $dictionary->name = $request->input('name');
        $dictionary->company_id = $request->input('companies');
        $dictionary->save();

        return redirect('dictionaries')->with('status', 'El diccionario ' . $dictionary->name . ' se ha actualizado satisfactoriamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dictionary  $dictionary
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dictionary $dictionary)
    {
        $dictionary->delete();

        return redirect('dictionaries')->with('status', 'Diccionario eliminado correctamente');
    }

    public function clone(Request $request) {

        $validatedData = $request->validate([
            'name' => 'required',
            'id' => 'required'
        ]);

        $dictionary = Dictionary::find($request->id);
        $clone = $dictionary->replicate(['created_at', 'updated_at']);
        $clone->name = $request->name;
        $clone->save();

        foreach ($dictionary->competences as $key => $competence) {
            $comp_clone = $competence->replicate();
            $comp_clone->dictionary_id = $clone->id;
            $comp_clone->push();

            foreach ($competence->levels as $key => $level) {
                $lev_clone = $level->replicate();
                $lev_clone->dictionary_competence_id = $comp_clone->id;
                $lev_clone->push();
            }
        }



        return redirect('dictionaries')->with('status', 'El diccionario ' . $dictionary->name . ' ha sido clonado');
    }

}

<?php

namespace App\Http\Controllers;

use App\Project;
use App\Company;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::paginate(15);
        return view('projects.index', ['projects' => $projects]);
    }

    public function index_json(Request $request) {

        $company = Company::find($request->id);
        return $company->projects->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'company_id' => 'required',
            'dictionaries' => 'required'
        ]);

        $project = new Project();
        $project->name = $request->input('name');
        $project->company_id = $request->input('company_id');
        $project->save();

        $project->dictionaries()->attach($request->input('dictionaries'));

        return redirect('projects')->with('status', 'Proyecto ' . $project->name . ' creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        return view('projects.create', ['project' => $project]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'company_id' => 'required',
            'dictionaries' => 'required'
        ]);

        $project->name = $request->input('name');
        $project->company_id = $request->input('company_id');
        $project->save();

        $project->dictionaries()->detach();
        $project->dictionaries()->attach($request->input('dictionaries'));

        return redirect('projects')->with('status', 'Proyecto ' . $project->name . ' actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $project->delete();
        return redirect('projects')->with('status', 'Proyecto ' . $project->name . ' eliminado correctamente');
    }
}

<?php

namespace App\Http\Controllers;

use App\Questionary;
use Illuminate\Http\Request;

class QuestionaryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questionaries = Questionary::paginate(15);
        return view('questionaries.index', ['questionaries' => $questionaries]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('questionaries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
        ]);

        $questionary = new Questionary();
        $questionary->name = $request->input('name');
        if($request->has('description')) {
            $questionary->description = $request->input('description');
        }

        $questionary->save();

        return redirect('questionaries')->with('status', 'Cuestionario ' . $questionary->name . ' creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Questionary  $questionary
     * @return \Illuminate\Http\Response
     */
    public function show(Questionary $questionary)
    {
        return view('questionaries.show', ['questionary' => $questionary]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Questionary  $questionary
     * @return \Illuminate\Http\Response
     */
    public function edit(Questionary $questionary)
    {
        return view('questionaries.create', ['questionary' => $questionary]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Questionary  $questionary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Questionary $questionary)
    {
        $validatedData = $request->validate([
            'name' => 'required',
        ]);

        $questionary->name = $request->input('name');
        if($request->has('description')) {
            $questionary->description = $request->input('description');
        }

        $questionary->save();

        return redirect('questionaries')->with('status', 'Cuestionario ' . $questionary->name . ' creado correctamente');
    }

    public function clone(Request $request) {
        $validatedData = $request->validate([
            'name' => 'required',
            'id' => 'required'
        ]);

        $questionary = Questionary::find($request->id);
        $clone = $questionary->replicate(['created_at', 'updated_at']);
        $clone->name = $request->name;
        $clone->save();

        foreach ($questionary->evidences as $key => $evidence) {
            $ev_clone = $evidence->replicate();
            $ev_clone->questionary_id = $clone->id;
            $ev_clone->push();
        }

        return redirect('questionaries')->with('status', 'El cuestionario ' . $questionary->name . ' ha sido clonado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Questionary  $questionary
     * @return \Illuminate\Http\Response
     */
    public function destroy(Questionary $questionary)
    {
        $questionary->delete();
        return redirect('questionaries')->with('status', 'Cuestionario ' . $questionary->name . ' eliminado correctamente');
    }
}

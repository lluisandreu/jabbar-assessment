<?php

namespace App\Http\Controllers;

use App\Evidence;
use App\EvidenceArea;
use Validator;
use Illuminate\Http\Request;

class EvidenceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function index_json(Request $request) {
        $evidences = Evidence::where('indicator_id', $request->id)->get();
        return $evidences->toJson();
    }

    public function indicators_json(Request $request) {

        $area = EvidenceArea::find($request->id);
        return $area->indicators->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('evidences.create', ['questionary' => $id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'weighing.required' => 'El campo de ponderación es obligatorio',
            'weighing.max' => 'El valor máximo del campo de ponderación es 100',
            'weighing.min' => 'El valor mínimo del campo de ponderación es 0',
        ];

        $validator = Validator::make($request->all(), [
            'body' => 'required|min:5',
            'area' => 'required',
            'indicator' => 'required',
            'weighing' => 'required|max:100|min:0',
            'descriptions' => 'required',
            'values' => 'required',
            'questionary' => 'required'
        ], $messages);

        $descriptions = explode(", ", $request->input('descriptions'));
        $values = explode(", ", $request->input('values'));

        $validator->after(function ($validator) use($request) {
            $descriptions = explode(", ", $request->input('descriptions'));
            $values = explode(", ", $request->input('values'));
            if (count($descriptions) !== count($values)) {
                $validator->errors()->add('descriptions', 'Las descripciones deben tener la misma cantidad de valores');
            }
        });

        if($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $descriptions = explode(", ", $request->input('descriptions'));
        $values = explode(", ", $request->input('values'));
        $combined = array_combine($descriptions, $values);

        $evidence = new Evidence;
        $evidence->body = $request->input('body');
        $evidence->questionary_id = $request->input('questionary');
        $evidence->indicator_id = $request->input('indicator');
        $evidence->weighing = $request->input('weighing');
        $evidence->valuations = json_encode($combined);
        $evidence->save();

        $area = \App\EvidenceArea::find($request->input('area'));

        return redirect('questionaries/' . $evidence->questionary_id . '#' . strtolower($area->name))->with('status', 'La evidencia ha sido creada correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Evidence  $evidence
     * @return \Illuminate\Http\Response
     */
    public function show(Evidence $evidence)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Evidence  $evidence
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $did)
    {   
        $evidence = Evidence::find($id);
        $values = array_values(json_decode($evidence->valuations, true));
        $keys = array_keys(json_decode($evidence->valuations, true));
        return view('evidences.create', ['evidence' => $evidence, 'values' => $values, 'keys' => $keys, 'questionary' => $did]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Evidence  $evidence
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Evidence $evidence)
    {
        $messages = [
            'weighing.required' => 'El campo de ponderación es obligatorio',
            'weighing.max' => 'El valor máximo del campo de ponderación es 100',
            'weighing.min' => 'El valor mínimo del campo de ponderación es 0',
        ];

        $validator = Validator::make($request->all(), [
            'body' => 'required|min:5',
            'area' => 'required',
            'indicator' => 'required',
            'weighing' => 'required|max:100|min:0',
            'questionary' => 'required'
        ], $messages);

        $validator->after(function ($validator) use($request) {
            $descriptions = explode(", ", $request->input('descriptions'));
            $values = explode(", ", $request->input('values'));
            if (count($descriptions) !== count($values)) {
                $validator->errors()->add('descriptions', 'Las descripciones deben tener la misma cantidad de valores');
            }
        });

        if($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $descriptions = explode(", ", $request->input('descriptions'));
        $values = explode(", ", $request->input('values'));

        $combined = array_combine($descriptions, $values);

        $evidence->body = $request->input('body');
        $evidence->questionary_id = $request->input('questionary');
        $evidence->indicator_id = $request->input('indicator');
        $evidence->weighing = $request->input('weighing');
        $evidence->valuations = json_encode($combined);
        $evidence->save();

        $area = \App\EvidenceArea::find($request->input('area'));

        return redirect('questionaries/' . $evidence->questionary_id . '#' . strtolower($area->name))->with('status', 'La evidencia ha sido actualizada correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Evidence  $evidence
     * @return \Illuminate\Http\Response
     */
    public function destroy(Evidence $evidence)
    {
        $area = $evidence->area->first();
        $id = $evidence->questionary_id;
        $evidence->delete();

        return redirect('questionaries/' . $id . '#' . strtolower($area->name))->with('status', 'La evidencia ha sido creada correctamente');
    }
}

<?php

namespace App\Http\Controllers;

use App\DictionaryCompetence;
use App\CompetenceLevel;
use App\Dictionary;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class DictionaryCompetenceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $dictionary = Dictionary::find($id);

        return view('competences.index', ['dictionary' => $dictionary, 'competences' => $dictionary->competences]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $dictionary = Dictionary::find($id);
        $competence = NULL;

        return view('competences.form', ['dictionary' => $dictionary, 'competence' => $competence, 'cid' => null]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id, $cid = NULL)
    {
        $validatedData = $request->validate([
            'name' => 'required',
        ]);

        if(!$cid) {
            $comp = new DictionaryCompetence();
            $comp->dictionary_id = $id;
        } else {
            $comp = DictionaryCompetence::find($cid);
        }

        $comp->name = $request->input('name');
        $comp->description = $request->input('description');
        $comp->skill_id = $request->input('skill');
        $comp->save();

        if($request->has('competences_hidden')) {
            $levels = $request->input('competences_hidden');
            $levels = json_decode($levels);

            $positions = DB::table('competence_levels')->get();

            if($positions->count()) {
                $last_position = DB::table('competence_levels')
                    ->select('weigth')
                    ->where('dictionary_competence_id', $comp->id)
                    ->max('weigth');
                $last_position++;
            } else {
                $last_position = 0;
            }
            
            foreach($levels as $level) {
                $level = $comp->levels()->create([
                    'dictionary_competence_id' => $comp->id,
                    'level' => $level->level,
                    'description' => $level->description,
                    'weigth' => $last_position
                ]); 
                $last_position++;
            }
        }

        return redirect()->route('dictionary_competences', ['id' => $id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DictionaryCompetence  $dictionaryCompetence
     * @return \Illuminate\Http\Response
     */
    public function show(DictionaryCompetence $dictionaryCompetence)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DictionaryCompetence  $dictionaryCompetence
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $cid)
    {
        $dictionary = Dictionary::find($id);
        $competence = DictionaryCompetence::find($cid);

        return view('competences.form', ['dictionary' => $dictionary, 'competence' => $competence, 'cid' => $cid]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DictionaryCompetence  $dictionaryCompetence
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DictionaryCompetence $dictionaryCompetence)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DictionaryCompetence  $dictionaryCompetence
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comp = DictionaryCompetence::find($id);
        $comp->delete();
        
        return back()->with('Competencia eliminada correctamente');
    }

    public function clone(Request $request) {

        $validatedData = $request->validate([
            'name' => 'required',
            'id' => 'required'
        ]);

        $competence = DictionaryCompetence::find($request->input('id'));

        if($competence) {
            $clone = $competence->replicate(['created_at', 'updated_at']);
            $clone->name = $request->name;
            $clone->save();

            foreach ($competence->levels as $key => $level) {
                $lev_clone = $level->replicate();
                $lev_clone->dictionary_competence_id = $clone->id;
                $lev_clone->push();
            }

            return back()->with('status', 'La competencia ' . $competence->name . ' ha sido clonada');
        } else {
            return back()->with('error', 'Ha habido un error al clonar la competencia');
        }
    }

    public function level_delete($id) {
        $level = CompetenceLevel::find($id);
        $level->delete();
        return back()->with('input', 'Nivel borrado satisfactoriamente');
    }

    public function level_edit(Request $request, $id) {


        $validatedData = $request->validate([
            'level' => 'required',
            'description' => 'required'
        ]);

        $level = CompetenceLevel::find($id);
        $level->level = $request->input('level');
        $level->description = $request->input('description');
        $level->save();

        return back()->with('input', 'Nivel editado satisfactoriamente');
        
    }

    public function level_sort(Request $request) {
        if($request->data) {
            foreach ($request->data as $key => $value) {
                $level = CompetenceLevel::find($value);
                $level->weigth = $key;
                $level->save();
            }
        }
    }

    public function levels_json(Request $request) {
        $levels = CompetenceLevel::where('dictionary_competence_id', $request->competence_id)->get();
        return $levels->toJson();
    }

    public function competences_json(Request $request) {
        $data = collect();
        $competences = DictionaryCompetence::where('dictionary_id', $request->dict_id)->get();
        return $competences->toJson();
    }
}

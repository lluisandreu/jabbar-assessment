<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dictionary extends Model
{
    public function company() {
    	return $this->belongsTo('App\Company');
    }

    public function competences() {
    	return $this->hasMany('App\DictionaryCompetence');
    }
}

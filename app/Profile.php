<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{

    protected $fillable = ['name', 'first_surname', 'second_surname', 'birthdate', 'telephone', 'email', 'province', 'city', 'linkedin', 'facebook', 'twitter', 'position_id', 'function', 'evaluation_date', 'company_id', 'project_id'];

    public function company() {
    	return $this->belongsTo('App\Company', 'company_id');
    }

    public function project() {
    	return $this->belongsTo('App\Project');
    }

    public function submission() {
    	return $this->hasOne('App\Submission');
    }

    public function position() {
    	return $this->belongsTo('App\CompanyPosition', 'position_id');
    }

    public function competences() {
    	return $this->position->competences;
    }

    public function provinceObject() {
        return $this->belongsTo('App\Province', 'province');
    }

    public function questionaryStatus() {
        if(isset($this->submission->evidences)) {
            $submissions = $this->submission->evidences;
            $evidences = \App\Evidence::all();
            $left = (count($evidences) + count($this->position->competences)) - count($submissions);
            return $left;
        } else {
            return NULL;
        }
    	
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EvidenceArea extends Model
{
    public $timestamps = false;

    public function indicators() {
    	return $this->hasMany('App\AreaIndicator');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    public function indicator() {
    	return $this->belongsTo('App\AreaIndicator', 'indicator_id');
    }
}

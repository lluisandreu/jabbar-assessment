<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evidence extends Model
{
    public function indicator() {
    	return $this->belongsTo('App\AreaIndicator', 'indicator_id');
    }

    public function area() {
    	return $this->hasManyThrough('App\EvidenceArea', 'App\AreaIndicator', 'id', 'id', 'indicator_id', 'evidence_area_id');
    }

    public function values() {
    	return array_keys(json_decode($this->valuations, true));
    }
}

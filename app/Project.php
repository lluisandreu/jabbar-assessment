<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public function dictionaries() {
    	return $this->belongsToMany('App\Dictionary', 'project_dictionaries_pivot', 'project_id', 'dictionary_id');
    }

    public function company() {
    	return $this->belongsTo('App\Company');
    }
}

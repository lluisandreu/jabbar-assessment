<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DictionaryCompetence extends Model
{

	protected $table = "dictionary_competences";
    protected $fillable = ['name', 'description', 'skill_id'];

    public function dictionary() {
    	return $this->belongsTo('App\Dictionary');
    }

    public function levels() {
    	return $this->hasMany('App\CompetenceLevel');
    }

    public function skill() {
    	return $this->belongsTo('App\Skill');
    }

    public function positions() {
        return $this->belongsToMany('App\CompanyPosition')->using('App\PositionCompetence');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubmissionEvidence extends Model
{
    protected $fillable = ['valuable_id', 'valuable_type', 'submission_id', 'type', 'indicator_id', 'result'];
    protected $table = 'submission_values';

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompetenceLevel extends Model
{
	protected $fillable = ['dictionary_competence_id', 'level', 'description', 'weigth'];

    public function competence() {
    	return $this->belongsTo('App\DictionaryCompetence');
    }
}

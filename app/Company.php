<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{

    public function sector() {
    	return $this->belongsTo('App\CompanySector', 'company_sector_id');
    }

    public function areas() {
    	return $this->hasManyThrough('App\CompanyArea', 'App\CompanyPosition', 'company_id', 'id', 'id', 'company_area_id')->distinct();
    }

    public function positions() {
    	return $this->HasMany('App\CompanyPosition');
    }

    public function dictionaries() {
    	return $this->hasMany('App\Dictionary');
    }

    public function competences() {
    	return $this->hasManyThrough('App\DictionaryCompetence', 'App\Dictionary');
    }

    public function projects() {
        return $this->hasMany('App\Project');
    }

    public static function sizes_array() {
        $sizes = array('0-49', '50-249', '250-999', '1000 o +');
        return $sizes;
    }

}

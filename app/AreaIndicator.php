<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AreaIndicator extends Model
{
    public $timestamps = false;

    public function area() {
    	return $this->belongsTo('App\EvidenceArea', 'evidence_area_id');
    }

    public function evidences() {
    	return $this->hasMany('App\Evidence', 'indicator_id');
    }

    public function skill() {
    	return $this->hasOne('App\Skill', 'indicator_id');
    }

    public function profileCompetences($profile_id) {
    	$competences = collect();
    	$profile = \App\Profile::find($profile_id);
    	if($this->skill) {
    		$competences = $profile->position->competences->where('skill_id', $this->skill->id);
    		return $competences;
    	}
    	return $competences;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionary extends Model
{
    public function evidences() {
    	return $this->hasMany('App\Evidence');
    }
}

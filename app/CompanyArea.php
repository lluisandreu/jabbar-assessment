<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyArea extends Model
{
    public function company() {
    	return $this->belongsToMany('App\Company', 'company_areas_pivot');
    }

    public function positions() {
    	return $this->hasMany('App\CompanyPosition');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
	protected $fillable = ['status'];

    public function evidences() {
    	return $this->hasMany('App\SubmissionEvidence');
    }
}

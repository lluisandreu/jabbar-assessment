<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyPosition extends Model
{
    public $timestamps = false;

    public function area() {
    	return $this->belongsTo('App\CompanyArea');
    }

    public function company() {
    	return $this->belongsTo('App\Company');
    }

    public function competences() {
        return $this->belongsToMany('App\DictionaryCompetence', 'position_competences')->withPivot('company_position_id', 'competence_level_id');
    }

    public function parent() {
        return $this->belongsTo('App\CompanyPosition', 'parent_id');
    }

    public function children() {
    	return $this->hasMany('App\CompanyPosition', 'parent_id');
    }

    public function getCompetences() {
        return $this->competences()->pluck('id');
    }
}

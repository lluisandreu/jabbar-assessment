@extends('layouts.assessment')

@section('content')
<div class="page">
	<div class="page-header">
		<h1 class="page-title">Hola {{ Auth::user()->name }}!</h1>
	</div>
	<div class="page-content">
		<div class="panel">
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
						<div class="panel">
							<div class="panel-heading">
								<h3 class="panel-heading">Mis perfiles</h3>
							</div>
							<div class="panel-content">
								Listado de perfiles
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

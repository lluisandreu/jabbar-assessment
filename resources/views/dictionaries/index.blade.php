@extends('layouts.assessment')

@section('content')
  <div class="page">
    <h1 class="page-header">Todos los diccionarios</h1>
    @if (count($errors) > 0)
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
    @endif
    <div class="container-fluid">
      <div class="row row-lg">
        <div class="col-xs-12 col-xl-12">
          <div class="dictionaries-panel panel panel-default">
            <div class="panel-body">
              <table class="table table-hover table-striped">
                <thead>
                  <tr>
                    <th>Diccionario</th>
                    <th>Empresa</th>
                    <th></th>
                  </tr>
                </thead>
                {{-- Not loading with ajax for now --}}
                <tbody>
                  @foreach($dictionaries as $dict)
                    <tr>
                      <td>{{ $dict->name }}</td>
                      <td>@isset($dict->company) {{ $dict->company->name }} @else <span class="badge">Sin empresa asignada</span>@endisset</td>
                      <td class="text-right">
                        <a href="{{ route('dictionary_competences', ['id' => $dict->id])}}" class="btn"><i class="fa fa-list"></i></a>
                        <button type="button" class="btn" data-toggle="modal" data-target="#dictionary-clone-modal" data-title="{{ $dict->name }}" data-id="{{ $dict->id }}"><i class="fa fa-clone"></i></button>
                        <a href="{{ url('dictionaries/'.$dict->id.'/edit') }}" class="btn"><i class="icon wb-pencil"></i></a>
                        <button type="button" class="btn" data-toggle="modal" data-target="#dictionary-modal" data-id="{{$dict->id}}"><i class="fa fa-trash"></i></button>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <a class="site-action btn-raised btn btn-success btn-floating" href="{{ url('dictionaries/create') }}">
      <i class="icon wb-plus" aria-hidden="true"></i>
    </a>
  </div>
@endsection

@section('modals')
<div id="dictionary-modal" class="modal fade in" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-simple" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Realmente quieres eliminar este diccionario?</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <form action="#" method="post" class="pull-right">
         {{ method_field('DELETE') }}
         {{ csrf_field() }}
         <button type="submit" class="btn btn-danger">Elimina</button>
        </form>
      </div>
    </div>
  </div>
</div>
<div id="dictionary-clone-modal" class="modal fade in" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-simple" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Clona este diccionario</h4>
      </div>
      <div class="modal-body">
        
        <form action="{{ route('dictionary_clone') }}" method="post" id="modal-clone">
          <div class="field-group">
            <label class="label-control">Nombre del nuevo diccionario</label>
            <input type="text" name="name" class="title form-control" value="">
            <input type="hidden" name="id" id="id" value="">
            {{ csrf_field() }}
            
          </div> 
        </form>
      </div>
      <div class="modal-footer">
        <button type="submit" form="modal-clone" class="btn">Clona</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('inline-scripts')
<script>
  $(document).ready(function() {
    $('#dictionary-modal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var id = button.data('id') // Extract info from data-* attributes
    var modal = $(this)
    modal.find('form').attr('action', '/dictionaries/' + id);
  });

    $('#dictionary-clone-modal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var id = button.data('id') // Extract info from data-* attributes
    var name = button.data('title')
    var modal = $(this)
    modal.find('.title').val('copia de ' + name);
    modal.find('#id').val(id);
  });
  
})

</script>
@endsection

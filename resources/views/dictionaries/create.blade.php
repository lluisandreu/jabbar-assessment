@extends('layouts.assessment')
@section('content')
<div class="page animsition">
  <div class="page-header">
    @isset($dictionary)
      <h1 class="page-title">Edita {{ $dictionary->name }} de {{ $dictionary->company->name }}</h1>
    @else 
      <h1 class="page-title">Crea una nuevo diccionario de competencias</h1>
    @endisset
  </div>
  <div class="page-content container-fluid">
    <div class="panel">
      <div class="panel-body">
        @include('dictionaries.form')
      </div>
    </div>  
  </div>
</div>
@endsection
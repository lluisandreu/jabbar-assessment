@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="{{ url('dictionaries') }}@isset($dictionary)/{{ $dictionary->id }}@endisset" id="dictionary-form" method="post" accept-charset="utf-8">
  @isset($dictionary)
    {{ method_field('PUT') }}
  @endisset
{{ csrf_field() }}
<div class="row">
 <div class="col-md-6">
   <div class="form-group">
     <label>Nombre del diccionario</label>
     <input type="text" class="form-control" name="name" @isset($dictionary) value="{{ old('name', isset($dictionary->name)?$dictionary->name:'') }}" @endisset data-validation="required">
   </div>
    <div class="form-group">
      <button type="submit" class="btn btn-primary">@isset($dictionary)Guarda cambios @else Añadir @endisset</button>
    </div>
 </div>
 <div class="col-md-6">
   <div class="form-group">
     <label>Empresa</label>
     <select class="form-control" name="companies" data-validation="required">
        <option hidden selected disabled>- Selecciona -</option>
       @foreach(App\Company::all() as $company)
        <option value="{{ $company->id }}" @isset($dictionary) @if($dictionary->company_id == $company->id) selected @endif @endisset @if(old('companies') && old('companies') == $dictionary->id) selected @endif>{{ $company->name }}</option>
       @endforeach
     </select>
   </div>
 </div>
</form>

@section('inline-scripts')
  <script>
  $(document).ready(function() {
    $.validate({
      form: '#dictionary-form',
      addValidClassOnAll : true,
        lang: 'es',
    });
  });
  </script>
@endsection

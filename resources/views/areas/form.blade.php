@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form id="area-form" enctype="multipart/form-data" action="{{ url('areas') }}@isset($area)/{{ $area->id }}@endisset" method="POST" accept-charset="utf-8">
	@isset($area)
		{{ method_field('PUT') }}
		@endisset
	{{ csrf_field() }}
	<div class="row">
		<div class="col-md-12 col-lg-6">
			<div class="form-group ">
				<label class="label-control">Nombre</label>
				<input type="text" class="form-control" placeholder="Nombre de la área" id="name" name="name" value="{{ old('name', isset($area->name)?$area->name:'') }}" data-validation="required">
			</div>
			<div class="form-group ">
				<button type="submit" class="btn btn-primary">@isset($area)Actualiza @else Crea @endisset</button>
			</div>
		</div>
	</div>
</form>

@section('inline-scripts')
	<script>
	$(document).ready(function() {
		$.validate({
			form: '#area-form',
			addValidClassOnAll : true,
		    lang: 'es',
		});
	});
	</script>
@endsection
@extends('layouts.assessment')
@section('content')
<div class="page animsition">
  <div class="page-header">
    @isset($area)
      <h1 class="page-title">Edita {{ $area->name }}</h1>
    @else 
      <h1 class="page-title">Crea una nueva area</h1>
    @endisset
  </div>
  <div class="page-content container-fluid">
    <div class="panel">
      <div class="panel-body">
        @include('areas.form')
      </div>
    </div>  
  </div>
</div>
@endsection
@extends('layouts.assessment-out')
@push('styles')
    <link rel="stylesheet" href="/vendor/remark/assets/examples/css/pages/login-v3.css">
@endpush
@section('content')
<div class="page vertical-align text-center">
  <div class="page-content vertical-align-middle">
    <div class="panel">
        <div class="panel-body">
            <div class="brand">
              <h2>Assessment</h2>
            </div>
            <p>Utiliza tus credenciales para entrar</p>
             <form class="form" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
              <div class="form-group">
                <label class="sr-only" for="email">Email</label>
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Email">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group">
                <label class="sr-only" for="password">Password</label>
                <input id="password" type="password" class="form-control" name="password" required placeholder="Pasword">
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group clearfix">
                <div class="checkbox-custom checkbox-inline checkbox-primary float-left">
                  <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                  <label for="remember">Recúerdame</label>
                </div>
                <a class="float-right" href="{{ route('password.request') }}">Olvidaste tu contraseña?</a>
              </div>
              <button type="submit" class="btn btn-primary btn-block">Entrar</button>
            </form>
            <p>No tienes una cuenta? <a href="{{ route('register') }}">Regístrate</a></p>

            <footer class="page-copyright page-copyright-inverse">
              <p>BY Bros Group</p>
              <p>© {{ date('Y') }}. All RIGHT RESERVED.</p>
            </footer>
        </div>
    </div>
  </div>
</div>
@endsection

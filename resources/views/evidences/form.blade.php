@push('styles')
  <link rel="stylesheet" type="text/css" href="/vendor/remark/global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.min.css">
@endpush

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form id="evidence-form" enctype="multipart/form-data" action="{{ url('evidences') }}@isset($evidence)/{{ $evidence->id }}@endisset" method="POST" accept-charset="utf-8">
	@isset($evidence)
		{{ method_field('PUT') }}
		@endisset
	{{ csrf_field() }}
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label>Evidencia</label>
				<textarea class="form-control" name="body" id="body" rows="4" placeholder="Evidencia" data-validation="length" data-validation-length="min10">{{ old('body', isset($evidence->body)?$evidence->body:'') }}</textarea>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
			<label for="area">Área</label>
				<select name="area" id="area" class="form-control" data-validation="required">
					<option value="" hidden selected disabled>Selecciona área</option>
					@foreach(App\EvidenceArea::all() as $area)
						<option value="{{ $area->id }}" @isset($evidence->indicator_id) @if($evidence->indicator->area->id == $area->id) selected @endif @endisset @if(old('area') && old('area') == $area->id) selected @endif>{{ $area->name }}</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
			<label for="indicator">Indicador</label>
				<select name="indicator" id="indicator" class="form-control" data-validation="required" readonly>
					@isset($evidence->indicator_id)
						@foreach(App\AreaIndicator::where('evidence_area_id', $evidence->indicator->area->id)->get() as $indicator)
							<option value="{{ $indicator->id }}" @if($evidence->indicator_id == $indicator->id) selected @endif>{{ $indicator->name }}</option>
							option
						@endforeach
					@else 
						<option value="" hidden selected disabled>Selecciona indicador</option>
					@endif
				</select>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="weighing">Ponderación (0-100)</label>
				<input type="number" class="form-control" name="weighing" id="weighing" value="{{ old('weighing', isset($evidence->weighing)?$evidence->weighing:'') }}" min="0" max="100" data-validation="required, number" data-validation-allowing="range[0;100]">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label for="descriptions">Descripciones</label>
				<input type="text" class="form-control" name="descriptions" id="descriptions" data-validation="required">
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="values">Valores numéricos</label>
				<input type="text" class="form-control" name="values" id="values" data-inputType="number" data-validation="required">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<input type="hidden" name="questionary" value="{{ $questionary }}">
				<button type="submit" id="form-submit" class="btn btn-primary">@isset($evidence)Actualiza @else Crea @endisset</button>
			</div>
		</div>
	</div>
</form>

@section('scripts')
<script src="/vendor/remark/global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.min.js" type="text/javascript"></script>
@endsection

@section('inline-scripts')
	<script>
		$(document).ready(function() {

			$.validate({
			    form: '#evidence-form',
			    addValidClassOnAll : true,
			      lang: 'es',
			      modules : 'date, security, location',
			      onModulesLoaded : function() {
			 
			        // Show strength of password
			        //$('input[name="password"]').displayPasswordStrength();
			       }
			  });

			var area = $('#area');
			area.on('change', function(event) {
				$('#indicator').html('');

				event.preventDefault();

				id = $(this).val();

				axios.get('/evidenceindicators/json', {
					params: {
						id: id
					}
				})
				.then(function (response) {
				   //console.log(response);
				   $('#indicator').removeAttr("readonly");
				   for (var i = response.data.length - 1; i >= 0; i--) {
				   	$('#indicator').append('<option value="' + response.data[i].id + '">' + response.data[i].name + '</option>');
				   };
				})
				.catch(function (error) {
				  console.log(error);
				});
			});

			$('#descriptions').tokenfield();
			$('#values').tokenfield();

			@isset($values)
				$('#values').tokenfield('setTokens', {!! json_encode($values) !!});
			@else 
				var i = 0;
				$('#descriptions').on('tokenfield:createdtoken', function(e){
					i++;
					$('#values').tokenfield('createToken', i.toString());
				});
			@endisset

			@isset($keys)
				$('#descriptions').tokenfield('setTokens', {!! json_encode($keys) !!});
			@endisset

			@if(old('indicator'))
				$('#indicator').html('');
				$('#indicator').append('<option value="' + '{!! old('indicator') !!}' + '">' + '{{ App\AreaIndicator::find(old('indicator'))->name }}' + '</option>');
			@endif

			@if(old('descriptions'))
				 var descriptions = "{!! old('descriptions') !!}";
				$('#descriptions').tokenfield('setTokens', descriptions.split(','));
			@endif

			@if(old('values'))
				 var values = "{!! old('values') !!}";
				$('#values').tokenfield('setTokens', values.split(','));
			@endif

		});
	</script>
@endsection
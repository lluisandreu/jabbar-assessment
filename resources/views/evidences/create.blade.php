@extends('layouts.assessment')
@section('content')
<div class="page animsition">
  <div class="page-header">
    @isset($evidence)
      <h1 class="page-title">Edita</h1>
    @else 
      <h1 class="page-title">Crea evidencia</h1>
    @endisset
  </div>
  <div class="page-content container-fluid">
    <div class="panel">
      <div class="panel-body">
        @include('evidences.form')
      </div>
    </div>  
  </div>
</div>
@endsection
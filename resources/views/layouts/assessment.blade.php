<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap material admin template">
  <meta name="author" content="">
  <title>Jabbar Assessment</title>

  <link rel='stylesheet' href='{{ asset('css/app.css') }}' />
  <link rel="stylesheet" href="/vendor/remark/global/css/bootstrap-extend.min.css" />

  <link rel="stylesheet" href="/vendor/remark/assets/css/site.min.css" />
  <link rel="stylesheet" href="//use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
  <link rel="stylesheet" href="/vendor/remark/global/fonts/material-design/material-design.min.css" />
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" />
  <link rel='stylesheet' href='//fonts.googleapis.com/css?family=Roboto:300,400,500,300italic' />
  
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/extensions/filter-control/bootstrap-table-filter-control.css">

  @stack('styles')
</head>

<body class="animsition site-navbar-small ">
	@include('layouts.topbar')
	@include('layouts.nav')
	  <!--[if lt IE 8]>
	        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	    <![endif]-->
	@yield('content')
	@yield('modals')

	<script src="/vendor/remark/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
	<script src="/vendor/remark/global/vendor/popper-js/umd/popper.min.js"></script>
	<script src="/vendor/remark/global/vendor/bootstrap/bootstrap.min.js"></script>
	<script src="/vendor/remark/global/vendor/animsition/animsition.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/extensions/filter-control/bootstrap-table-filter-control.min.js"></script>
	<script src="{{ asset('js/app.js') }}"></script>

	@yield('scripts')
	@yield('inline-scripts')

@if (session('status'))
	<script>
		$(function() {        
	      toastr.success('{{ session('status') }}')
	   });
	</script>
@endif
</body>
</html>
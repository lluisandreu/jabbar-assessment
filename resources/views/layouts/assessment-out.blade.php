<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap material admin template">
  <meta name="author" content="">
  <title>Jabbar Assessment</title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
  <link rel="stylesheet" href="/vendor/remark/global/css/bootstrap.min.css" />
  <link rel="stylesheet" href="/vendor/remark/global/css/bootstrap-extend.min.css" />
  <link rel="stylesheet" href="/vendor/remark/assets/css/site.min.css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
  <link rel="stylesheet" href="/vendor/remark/global/fonts/web-icons/web-icons.min.css" />
  <link rel="stylesheet" href="/vendor/remark/global/fonts/brand-icons/brand-icons.min.css" />
  <link rel="stylesheet" href="/vendor/remark/global/fonts/glyphicons/glyphicons.css" />
  <link rel="stylesheet" href="/vendor/remark/global/fonts/material-design/material-design.min.css" />
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" />
  <link rel='stylesheet' href='//fonts.googleapis.com/css?family=Roboto:300,400,500,300italic' />

  @stack('styles')

  <script src="/vendor/remark/global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  	Breakpoints();
  </script>
</head>

<body class="animsition site-navbar-small page-login-v3 layout-full">

	@yield('content')

	<script src="/vendor/remark/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
	<script src="/vendor/remark/global/vendor/jquery/jquery.js"></script>
	
	<script src="/vendor/remark/global/vendor/popper-js/umd/popper.min.js"></script>
	<script src="/vendor/remark/global/vendor/bootstrap/bootstrap.min.js"></script>
	<script src="/vendor/remark/global/vendor/animsition/animsition.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
	<script src="/vendor/remark/global/js/State.js"></script>
	<script src="/vendor/remark/global/js/Component.js"></script>
	<script src="/vendor/remark/global/js/Plugin.js"></script>
	<script src="/vendor/remark/global/js/Base.js"></script>
	<script src="/vendor/remark/global/js/Config.js"></script>
	<script src="/vendor/remark/assets/js/Section/Menubar.js"></script>
	<script src="/vendor/remark/assets/js/Section/Sidebar.js"></script>
	<script src="/vendor/remark/assets/js/Section/PageAside.js"></script>
	<script src="/vendor/remark/assets/js/Plugin/menu.js"></script>
	<script src="/vendor/remark/assets/js/Site.js"></script>

	 @stack('scripts')

	<script>
  		Config.set('assets', '/vendor/remark/assets');
  	</script>

  	  @if (session('status'))
<script>
	$(function() {        
	      toastr.success('{{ session('status') }}')
	   });
</script>
@endif
</body>
</html>
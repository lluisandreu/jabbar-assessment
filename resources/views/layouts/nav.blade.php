<div class="site-menubar site-menubar-light">
  <div class="site-menubar-body">
    <div>
      <div>
        <ul class="site-menu" data-plugin="menu">
          <li class="site-menu-item">
            <a href="/"><i class="fa fa-home"></i></a>
          </li>
          </li>
          @if(Entrust::can('admin.profiles'))
            <li class="dropdown site-menu-item has-sub">
              <a data-toggle="dropdown-menu" href="javascript:void(0)">
                <i class="fa fa-user" aria-hidden="true"></i>
                <span class="site-menu-title">Perfiles</span>
                <span class="site-menu-arrow"></span>
              </a>
              <div class="dropdown-menu">
                <div class="site-menu-scroll-wrap is-list">
                  <div>
                    <div>
                      <ul class="site-menu-sub site-menu-normal-list">
                        <li class="site-menu-item">
                          <a class="animsition-link" href="/profiles">
                            <span class="site-menu-title">Listado de perfiles</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="/profiles/create">
                            <span class="site-menu-title">Crea un perfil</span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </li>
          @endif
          @if(Entrust::can('admin.users'))
            <li class="dropdown site-menu-item has-sub">
              <a data-toggle="dropdown-menu" href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon wb-users" aria-hidden="true"></i>
                <span class="site-menu-title">Usuarios</span>
                <span class="site-menu-arrow"></span>
              </a>
              <div class="dropdown-menu">
                <div class="site-menu-scroll-wrap is-list">
                  <div>
                    <div>
                      <ul class="site-menu-sub site-menu-normal-list">
                        <li class="site-menu-item">
                          <a class="animsition-link" href="/users">
                            <span class="site-menu-title">Usuarios</span>
                          </a>
                        </li>
                         <li class="site-menu-item">
                          <a class="animsition-link" href="/users/create">
                            <span class="site-menu-title">Crea un usuario</span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </li>
          @endif
          @if(Entrust::can('admin.companies'))
            <li class="dropdown site-menu-item has-sub">
              <a data-toggle="dropdown-menu" href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="fa fa-building" aria-hidden="true"></i>
                <span class="site-menu-title">Empresas</span>
                <span class="site-menu-arrow"></span>
              </a>
              <div class="dropdown-menu">
                <div class="site-menu-scroll-wrap is-list">
                  <div>
                    <div>
                      <ul class="site-menu-sub site-menu-normal-list">
                        <li class="site-menu-item">
                          <a class="animsition-link" href="/companies">
                            <span class="site-menu-title">Empresas</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="/companies/create">
                            <span class="site-menu-title">Crea una empresa</span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li class="dropdown site-menu-item has-sub">
              <a data-toggle="dropdown-menu" href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <span class="site-menu-title">Áreas</span>
                <span class="site-menu-arrow"></span>
              </a>
              <div class="dropdown-menu">
                <div class="site-menu-scroll-wrap is-list">
                  <div>
                    <div>
                      <ul class="site-menu-sub site-menu-normal-list">
                        <li class="site-menu-item">
                          <a class="animsition-link" href="/areas">
                            <span class="site-menu-title">Áreas</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="/areas/create">
                            <span class="site-menu-title">Crea una área</span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li class="dropdown site-menu-item has-sub">
              <a data-toggle="dropdown-menu" href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="fa fa-book" aria-hidden="true"></i>
                <span class="site-menu-title">Diccionarios</span>
                <span class="site-menu-arrow"></span>
              </a>
              <div class="dropdown-menu">
                <div class="site-menu-scroll-wrap is-list">
                  <div>
                    <div>
                      <ul class="site-menu-sub site-menu-normal-list">
                        <li class="site-menu-item">
                          <a class="animsition-link" href="/dictionaries">
                            <span class="site-menu-title">Diccionarios</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="/dictionaries/create">
                            <span class="site-menu-title">Crea una diccionario</span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li class="dropdown site-menu-item has-sub">
              <a data-toggle="dropdown-menu" href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="fa fa-archive" aria-hidden="true"></i>
                <span class="site-menu-title">Proyectos</span>
                <span class="site-menu-arrow"></span>
              </a>
              <div class="dropdown-menu">
                <div class="site-menu-scroll-wrap is-list">
                  <div>
                    <div>
                      <ul class="site-menu-sub site-menu-normal-list">
                        <li class="site-menu-item">
                          <a class="animsition-link" href="/projects">
                            <span class="site-menu-title">Proyectos</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="/projects/create">
                            <span class="site-menu-title">Crea un proyecto</span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li class="dropdown site-menu-item has-sub">
              <a data-toggle="dropdown-menu" href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="fa fa-question" aria-hidden="true"></i>
                <span class="site-menu-title">Cuestionarios</span>
                <span class="site-menu-arrow"></span>
              </a>
              <div class="dropdown-menu">
                <div class="site-menu-scroll-wrap is-list">
                  <div>
                    <div>
                      <ul class="site-menu-sub site-menu-normal-list">
                        <li class="site-menu-item">
                          <a class="animsition-link" href="/questionaries">
                            <span class="site-menu-title">Cuestionarios</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="/questionaries/create">
                            <span class="site-menu-title">Crea un nuevo cuestionario</span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </li>
          @endif
        </ul>
      </div>
    </div>
  </div>
</div>


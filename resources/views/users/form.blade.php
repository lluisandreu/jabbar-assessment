@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form enctype="multipart/form-data" id="user-form" action="{{ url('users') }}@isset($user)/{{ $user->id }}@endisset" method="POST" accept-charset="utf-8">
	@isset($user)
		{{ method_field('PUT') }}
		@endisset
	{{ csrf_field() }}
	<div class="row">
		<div class="col-md-12 col-lg-6">
			<div class="form-group">
				<label class="label-control">Nombre</label>
				<input type="text" class="form-control" placeholder="Nombre" id="name" name="name" value="{{ old('name', isset($user->name)?$user->name:'') }}" data-validation="required">
			</div>
			<div class="form-group">
				<label class="label-control">Email</label>
				<input type="email" class="form-control" placeholder="Email" id="email" name="email" value="{{ old('email', isset($user->email)?$user->email:'') }}" @isset($user->email) readonly @endisset data-validation="email">
			</div>
			<div class="form-group">
				<label class="label-control">Contraseña</label>
				<input type="password" class="form-control" placeholder="Contraseña" id="password" name="password" data-validation="alphanumeric, length, strength" data-validation-length="min6" data-validation-strength="2" @isset($user) data-validation-optional="true" @endisset data-validation-strength="1">
			</div>
			<div class="form-group">
				<label class="label-control">Repite tu contraseña</label>
				<input type="password" class="form-control" placeholder="Repite tu contraseña" id="password_2" name="password_confirmation" data-validation="alphanumeric, length" data-validation-length="min6" @isset($user) data-validation-optional="true" @endisset>
			</div>
			<div class="form-group checkbox-custom">
				<input type="checkbox" name="status" value="1" @isset($user) @if($user->status == '1') checked @endif @else checked @endisset>
				<label class="label-control">Activo</label>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary">@isset($user)Actualiza @else Crea @endisset</button>
			</div>
		</div>
		<div class="col-md-12 col-lg-6">
			<div class="form-group">
		    	<label class="control-label">Avatar</label>
		    	@isset($user->avatar)
		    		<img src="{{  asset('storage/avatar/' . $user->avatar) }}">
		    	@endisset
	    	</div>
	    	<div class="form-group ">
	    		<input type="file" class="dropify form-control" id="avatar" name="avatar" data-plugin="dropify" data-height="300">
	    	</div>
			<div class="panel panel-primary panel-line">
				<div class="panel-heading">
					<h3 class="panel-title">Empresa</h3>
				</div>
				<div class="panel-body">
					<div class="form-group">
						<label class="label-control">Elige una empresa para este usuario</label>
						<select name="company_id" class="form-control" id="company" data-validation="required">
							@foreach(App\Company::all() as $company)
								<option value="{{ $company->id }}" @isset($user) @if($user->company_id == $company->id)  selected @endif @endisset> {{ $company->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			<div class="panel panel-primary panel-line">
				<div class="panel-heading">
					<h3 class="panel-title">Roles</h3>
				</div>
				<div class="panel-body">
					<div class="form-group">
						<label class="label-control">Elige un rol para el usuario</label>
						<select name="roles" class="form-control" id="roles"  data-validation="required">
							@foreach(App\Role::all() as $role)
								<option value="{{ $role->id }}" @isset($user) @if($user->roles->isNotEmpty() && $role->id == $user->roles->get(0)->id) selected @endif @endisset> {{ $role->display_name }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

@section('inline-scripts')
	<script>
	$(document).ready(function() {
		$.validate({
			form: '#user-form',
			addValidClassOnAll : true,
		    lang: 'es',
		    modules : 'date, security, location',
		    onModulesLoaded : function() {
	 
		      var optionalConfig = {
			      fontSize: '1rem',
			      padding: '1px',
			      bad : 'Muy insegura',
			      weak : 'Insegura',
			      good : 'Segura',
			      strong : 'Muy segura',
			      weight: 'normal',
			      width: '100%'
			    };

			    $('input[name="password"]').displayPasswordStrength(optionalConfig);
		     }
		});
	});
	</script>
@endsection
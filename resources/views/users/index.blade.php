@extends('layouts.assessment')

@section('content')
  <div class="page">
    <div class="page-header">Todos los usuarios</div>
    @if (count($errors) > 0)
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
    @endif
    <div class="container-fluid">
      <div class="row row-lg">
        <div class="col-xs-12 col-xl-12">
          <div class="profiles-panel panel panel-default">
            <div class="panel-body">
              <table class="table table-hover table-striped">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Estado</th>
                    <th>Nombre</th>
                    <th>Email</th>
                    <th>Roles</th>
                    <th></th>
                  </tr>
                </thead>
                {{-- Not loading with ajax for now --}}
                <tbody>
                  @foreach($users as $user)
                    <tr>
                      <td>{{ $user->id }}</td>
                      <td>@if($user->status)<i class="fa fa-check"></i>@else<i class="fa fa-close"></i>@endif</td>
                      <td>{{ $user->name }}</td>
                      <td>{{ $user->email }}</td>
                      <td>@if($user->roles->isNotEmpty()) {{ $user->roles->get(0)->display_name }} @else No tiene roles @endif</td>
                      <td class="text-right">
                        <a href="{{ url('users/'.$user->id.'/edit') }}" class="btn"><i class="fa fa-edit"></i></a>
                        <button type="button" class="btn" data-toggle="modal" data-target="#user-modal" data-id="{{$user->id}}"><i class="fa fa-trash"></i></button>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <a class="site-action btn-raised btn btn-success btn-floating" href="{{ url('users/create') }}">
      <i class="icon wb-plus" aria-hidden="true"></i>
    </a>
  </div>
@endsection

@section('modals')
<div id="user-modal" class="modal fade in" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-simple" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Realmente quieres eliminar este usuario?</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <form action="#" method="post" class="pull-right">
         {{ method_field('DELETE') }}
         {{ csrf_field() }}
         <button type="submit" class="btn btn-danger">Elimina</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

@section('inline-scripts')
<script>
  $(document).ready(function() {
    $('#user-modal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var id = button.data('id') // Extract info from data-* attributes
    var modal = $(this)
    modal.find('form').attr('action', '/users/' + id);
  });
  
})

</script>
@endsection

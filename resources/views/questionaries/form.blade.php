@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form id="questionary-form" enctype="multipart/form-data" action="{{ url('questionaries') }}@isset($questionary)/{{ $questionary->id }}@endisset" method="POST" accept-charset="utf-8">
	@isset($questionary)
		{{ method_field('PUT') }}
		@endisset
	{{ csrf_field() }}
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="form-group">
					<label class="label-control">Nombre</label>
					<input type="text" class="form-control" placeholder="Nombre del cuestionario" id="name" name="name" value="{{ old('name', isset($questionary->name)?$questionary->name:'') }}" data-validation="required">
				</div>
				<div class="form-group">
					<label class="label-control">Descripción</label>
					<textarea class="form-control" id="description" name="description">{{ old('name', isset($questionary->description)?$questionary->description:'') }}</textarea>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-8">
					<button type="submit" id="form-submit" class="btn btn-primary">@isset($questionary)Actualiza @else Crea @endisset</button>
				</div>
			</div>
		</div>
	</div>
</form>

@section('inline-scripts')
	<script>
		$(document).ready(function() {

			$.validate({
				form: '#questionary-form',
				addValidClassOnAll : true,
			    lang: 'es',
			    modules : 'html5, date, security, location',
			    onModulesLoaded : function() {
		 
			      // Show strength of password
			      //$('input[name="password"]').displayPasswordStrength();
			     }
			});
		});
	</script>
@endsection
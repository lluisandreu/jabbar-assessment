@extends('layouts.assessment')

@section('content')
  <div class="page">
    <h1 class="page-header">Gestión de cuestionario</h1>
    @if (count($errors) > 0)
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
    @endif
    <div class="container-fluid">
      <div class="row row-lg">
        <div class="col-xs-12 col-xl-12">
          <div class="profiles-panel panel panel-default">
            <div class="panel-body">
              <h3>Listado de evidencias</h3>
              <p><a href="{{ url('questionaries/'.$questionary->id .'/evidences/create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Crea una evidencia</a></p>
              <div id="questionary-tabs" class="nav-tabs-horizontal">
                  <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item" role="presentation"><a href="#all" class="nav-link active" data-area="all" aria-controls="all" role="tab">All</a></li>
                    <li class="nav-item" role="presentation"><a href="#knowledge" class="nav-link" data-area="knowledge" aria-controls="knowledge" role="tab">Knowledge</a></li>
                    <li class="nav-item" role="presentation"><a href="#engagement" class="nav-link" data-area="engagement" aria-controls="engagement" role="tab">Engagement</a></li>
                    <li class="nav-item" role="presentation"><a href="#potential" class="nav-link" data-area="potential" aria-controls="potential" role="tab">Potential</a></li>
                  </ul>
                  <div class="content">
                    <div class="" role="tabpanel">
                      @isset($questionary->evidences)
                        <table class="table">
                          <thead>
                            <th>Área</th>
                            <th>Indicador</th>
                            <th>Evidencia</th>
                            <th>Valores</th>
                            <th>Ponderación</th>
                            <th class="text-right">Acciones</th>
                          </thead>
                          <tbody>
                              @foreach($questionary->evidences as $ev)
                                <tr class="evidence-row" data-area="{{ strtolower($ev->area->first()->name) }}">
                                  <td>{{ $ev->area->first()->name }}</td>
                                  <td>{{ $ev->indicator->name }}</td>
                                  <td style="max-width: 500px">{{ $ev->body }}</td>
                                  <td>{{ implode(', ', $ev->values()) }}</td>
                                  <td>{{ $ev->weighing }}%</td>
                                  <td class="text-right">
                                    <a href="{{ url('questionaries/'.$questionary->id .'/evidences/'.$ev->id.'/edit') }}" class="btn"><i class="icon wb-pencil"></i></a>
                                    <button type="button" id="rm-modal" class="btn" data-toggle="modal" data-target="#evidence-modal" data-id="{{$ev->id}}"><i class="fa fa-trash"></i></button>
                                  </td>
                                </tr>
                              @endforeach
                          </tbody>
                        </table>
                      @endisset 
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <a class="site-action btn-raised btn btn-success btn-floating" href="{{ url('questionaries/'.$questionary->id .'/evidences/create') }}">
      <i class="icon wb-plus" aria-hidden="true"></i>
    </a>
  </div>

@endsection

@section('modals')
<div id="evidence-modal" class="modal fade in" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-simple" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Realmente quieres eliminar esta evidencia?</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <form action="#" method="post" class="pull-right">
         {{ method_field('DELETE') }}
         {{ csrf_field() }}
         <button type="submit" class="btn btn-danger">Elimina</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

@section('inline-scripts')
  <script>
    $(document).ready(function() {
      var hash = window.location.hash;
      hash = hash.split('#')[1];
      console.log(hash);
      if(typeof hash != 'undefined') {
        $('#questionary-tabs').find('.nav-link').removeClass('active');
        if(hash == 'all') {
          $('.evidence-row').show('50');
        } else {
          $('.evidence-row[data-area="' + hash + '"]').show('50');
          $('#questionary-tabs').find('.nav-link[data-area="' + hash + '"]').addClass('active');
          $('.evidence-row').not("[data-area='" + hash + "']").hide('50');
        }
      }
      
      $('#questionary-tabs').find('.nav-link').on('click', function(event) {
        event.preventDefault();
        var id = $(this).data('area');
        $('#questionary-tabs').find('.nav-link').removeClass('active');
        $(this).addClass('active');
        $('.evidence-row[data-area="' + id + '"]').show('50');
        $('.evidence-row').not("[data-area='" + id + "']").hide('50');
        if(id == 'all') {
          $('.evidence-row').show('50');
        }
      });

      $('#evidence-modal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var id = button.data('id') // Extract info from data-* attributes
        var modal = $(this)
        modal.find('form').attr('action', '/evidences/' + id);
      });
     });
  </script>
  
@endsection


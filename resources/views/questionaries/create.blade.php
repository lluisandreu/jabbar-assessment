@extends('layouts.assessment')
@section('content')
<div class="page animsition">
  <div class="page-header">
    @isset($questionary)
      <h1 class="page-title">Edita {{ $questionary->name }}</h1>
    @else 
      <h1 class="page-title">Crea un nuevo cuestionario</h1>
    @endisset
  </div>
  <div class="page-content container-fluid">
    <div class="panel">
      <div class="panel-body">
        @include('questionaries.form')
      </div>
    </div>  
  </div>
</div>
@endsection
@extends('layouts.assessment')
@push('styles')
  <link rel="stylesheet" href="/vendor/remark/global/vendor/jquery-ui/jquery-ui.min.css">
@endpush
@section('content')
<div class="page">
  <h1 class="page-header">
    @isset($competence)
      Edita {{ $competence->name }}
    @else 
      Añadir competencia al diccionario {{ $dictionary->name }}
    @endisset
  </h1>
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ url('dictionaries') }}">Diccionarios</a></li>
    <li class="breadcrumb-item"><a href="{{ url('dictionaries/'.$dictionary->id.'/competences') }}">Competencias del diccionario {{ $dictionary->name }}</a></li>
    <li class="breadcrumb-item active">
      @isset($competence)Edita {{ $competence->name }} @else Añadir competencia al diccionario {{ $dictionary->name }} @endisset</li>
  </ol>
  @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif
  <div class="container-fluid">
    <div class="row">
      <div class="col-xs-12 col-xl-12">
        <div class="panel panel-transparent">
          <div class="nav-tabs-horizontal nav-tabs-inverse" data-plugin="tabs">
              <ul class="nav nav-tabs nav-tabs-solid" role="tablist">
                <li class="nav-item" role="presentation">
                  <a class="nav-link active" data-toggle="tab" href="#competence" aria-controls="competence" role="tab">
                  Competencia
                </a>
                </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="competence" role="tabpanel">
                  <form action="{{ route('dictionary_competences_post', ['id' => $dictionary->id, 'cid' => $cid]) }}" method="post" accept-charset="utf-8" id="new-competence-form">
                    {{ csrf_field() }}
                    <div class="row">
                     <div class="col-md-3">
                       <div class="form-group">
                         <label>Nombre</label>
                         <input type="text" class="form-control" name="name" value="{{ old('name', isset($competence->name)?$competence->name:'') }}" data-validation="required">
                         <input type="hidden" id="input_competendes" name="competences_hidden">
                       </div>
                     </div>
                     <div class="col-md-5">
                       <div class="form-group">
                         <label>Definición</label>
                         <input type="text" class="form-control" name="description" value="{{ old('name', isset($competence->description)?$competence->description:'') }}" data-validation="required">
                       </div>
                     </div>
                     <div class="col-md-4">
                       <div class="form-group">
                         <label>Tipo de competencia</label>
                         <select class="form-control" name="skill" data-validation="required">
                          <option hidden disabled selected>Selecciona un tipo de competencia</option>
                           @foreach(App\Skill::all() as $skill)
                            <option value="{{ $skill->id }}" @isset($competence) @if($competence->skill_id == $skill->id) selected @endif @endisset @if(old('skill') && old('skill') == $skill->id) selected @endif>{{ $skill->name }}</option>
                           @endforeach
                         </select>
                       </div>
                     </div>
                  </form>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    <div class="row">
      <div class="col-xs-12 col-xl-12">
        <div class="panel panel-transparent">
          <div class="nav-tabs-horizontal nav-tabs-inverse" data-plugin="tabs">
            <ul class="nav nav-tabs nav-tabs-solid" role="tablist">
              <li class="nav-item" role="presentation">
                <a class="nav-link active" data-toggle="tab" href="#competence" aria-controls="competence" role="tab">
                Niveles de la competencia
              </a>
              </li>
            </ul>
            <div class="tab-content" id="vue-competences">
              <div class="tab-pane active" id="competence" role="tabpanel">
                <form id="new-competence-level">
                  <div class="row">
                   <div class="col-md-4">
                     <div class="form-group">
                       <label class="sr-only">Nivel</label>
                       <input type="text" class="form-control" placeholder="Nivel" v-model="level">
                     </div>
                   </div>
                   <div class="col-md-7">
                     <div class="form-group">
                       <label class="sr-only">Descripción</label>
                       <input type="text" class="form-control" placeholder="Descripción" v-model="description" @keyup.enter="newLevel">
                     </div>
                   </div>
                   <div class="col-md-1">
                     <button type="button" id="add-level-button" v-on:click="newLevel" class="btn btn-primary">Añadir</button>
                   </div>
                </form>
              </div>
              <div class="levels-table">
                <div class="alert">@{{ errors }}</div>
                <table class="table" id="levels-table">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Nivel</th>
                      <th>Descripción</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    @isset($competence)
                      @foreach($competence->levels->sortBy('weigth') as $level)
                        <tr id="{{$level->id}}">
                          <td><a class="handle"><i class="fa fa-arrows"></i></a></td>
                          <td class="level">{{ $level->level }}</td>
                          <td class="description">{{ $level->description }}</td>
                          <td class="text-right">
                            <button class="btn btn-pure btn-default btn-outline" data-toggle="modal" data-target="#modal-edit-{{$level->id}}"><i class="fa fa-edit"></i></button>
                            <div id="modal-edit-{{$level->id}}" class="modal fade" tabindex="-1" role="dialog">
                                <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <form action="/levels/{{ $level->id }}/edit" method="post">
                                  {{ csrf_field() }}
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4>Edita este nivel</h4>
                                  </div>
                                  <div class="modal-body">
                                    <div class="row">
                                      <div class="col-md-6">
                                        <label>Nivel</label>
                                        <input type="text" class="form-control" name="level" value="{{ $level->level }}" required>
                                      </div>
                                      <div class="col-md-6">
                                        <label>Descripción</label>
                                        <input type="text" class="form-control" name="description" value="{{ $level->description }}" required>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Descarta</button>
                                    <button type="submit" class="btn btn-primary">Guarda</button>
                                  </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                            <button type="button" class="btn btn-pure btn-default btn-outline" data-toggle="modal" data-target="#modal-{{$level->id}}"><i class="icon wb-trash"></i></button>
                            <div id="modal-{{$level->id}}" class="modal fade" tabindex="-1" role="dialog">
                              <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title">Realmente quieres eliminar este nivel?</h4>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <form action="/levels/{{ $level->id }}/delete" method="post" class="pull-right">
                                   {{ csrf_field() }}
                                   <button type="submit" class="btn btn-danger">Elimina</button>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>
                        </td>
                        </tr>
                      @endforeach
                    @endisset
                    <tr v-if="rows" v-for="(row, index) in rows">
                      <td><input type="text" class="form-control" name="level[]" v-model="row.level"></td>
                      <td><input type="text" class="form-control" name="level[]" v-model="row.description"></td>
                      <td><button class="btn-pure btn-default btn-outline" v-on:click="removeRow(row.id)"><i class="fa fa-trash"></i></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="form-group">
                <button type="submit" v-on:click="submitForm" class="btn btn-primary">@isset($competence) Guardar @else Crear competencia @endisset</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

@section('scripts')
  <script src="/vendor/remark/global/vendor/jquery-ui/jquery-ui.min.js"></script>
@endsection

@section('inline-scripts')
<script>
  $.validate({
    form: '#new-competence-form',
    addValidClassOnAll : true,
      lang: 'es',
      modules : 'date, security, location',
      onModulesLoaded : function() {
 
        // Show strength of password
        //$('input[name="password"]').displayPasswordStrength();
       }
  });

  var app = new Vue({
    el: '#vue-competences',
    data: {
      level: '',
      description: '',
      rows: [],
      errors: '',
      rowId: 1,
    },
    methods: {
      newLevel: function() {
        this.rows.push({
          level: this.level,
          description: this.description
        });
        this.rowId += 1;
        this.level = ''; 
        this.description = '';
      },
      removeRow: function(id) {
        this.rows.splice(id, 1);
      },
      submitForm: function() {
          var data = JSON.stringify(this.rows);
          $('#input_competendes').val(data);
          $('#new-competence-form').submit();
      },
    },
  });

  $(document).ready(function() {
    $('#levels-table tbody').sortable({
      containment: 'parent',
      revert: true,
      handle: '.handle',
      axis: 'y',
      update: function(event, ui) {
        var data = {};
        data = $(this).sortable( "toArray" );
        axios.post('/levels/sort', {
          data: data
        }).then(function (response) {
          console.log(response);
        })
        .catch(function (error) {
          console.log(error);
        });
      }
    });
  });
  

</script>
@endsection

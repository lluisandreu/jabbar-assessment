@extends('layouts.assessment')

@section('content')
  <div class="page">
    <h1 class="page-header">Listado de competencias del diccionario {{ $dictionary->name }}</h1>
    @if (count($errors) > 0)
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
    @endif
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
      <li class="breadcrumb-item"><a href="{{ url('dictionaries') }}">Diccionarios</a></li>
      <li class="breadcrumb-item active"><a href="#"></a> Competencias del diccionario {{ $dictionary->name }}</li>
    </ol>
    <div class="container-fluid">
      <div class="row row-lg">
        <div class="col-xs-12 col-xl-12">
          <div class="dictionaries-panel panel panel-default">
            <div class="panel-body">
              <table class="table table-hover table-striped">
                <thead>
                  <tr>
                    <th>Competencia</th>
                    <th>Descripción</th>
                    <th>Tipo de compentencia</th>
                    <th></th>
                  </tr>
                </thead>
                {{-- Not loading with ajax for now --}}
                <tbody>
                  @foreach($competences as $comp)
                    <tr>
                      <td>{{ $comp->name }}</td>
                      <td>{{ $comp->description }}</td>
                      <td>{{ $comp->skill->name }}</td>
                      <td class="text-right">
                        <a href="/dictionaries/{{ $dictionary->id }}/competences/{{ $comp->id }}" class="btn"><i class="fa fa-list"></i></a>
                        <button type="button" class="btn" data-toggle="modal" data-target="#competence-clone-modal" data-title="{{ $comp->name }}" data-id="{{ $comp->id }}"><i class="fa fa-clone"></i></button>
                        <button type="button" class="btn" data-toggle="modal" data-target="#competence-modal" data-id="{{$comp->id}}"><i class="fa fa-trash"></i></button>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <a class="site-action btn-raised btn btn-success btn-floating" href="{{ route('dictionary_competences_create', ['id' => $dictionary->id]) }}">
      <i class="icon wb-plus" aria-hidden="true"></i>
    </a>
  </div>
@endsection

@section('modals')
<div id="competence-modal" class="modal fade in" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-simple" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Realmente quieres eliminar esta competencia?</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <form action="#" method="post" class="pull-right">
         {{ method_field('DELETE') }}
         {{ csrf_field() }}
         <button type="submit" class="btn btn-danger">Elimina</button>
        </form>
      </div>
    </div>
  </div>
</div>
<div id="competence-clone-modal" class="modal fade in" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-simple" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Clona esta competencia</h4>
      </div>
      <div class="modal-body">
        
        <form action="{{ route('competences_clone') }}" method="post" id="modal-clone">
          <div class="field-group">
            <label class="label-control">Nombre de la nueva competencia</label>
            <input type="text" name="name" class="title form-control" value="">
            <input type="hidden" name="id" id="id" value="">
            {{ csrf_field() }}
            
          </div> 
        </form>
      </div>
      <div class="modal-footer">
        <button type="submit" form="modal-clone" class="btn">Clona</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('inline-scripts')
<script>
  $(document).ready(function() {
    $('#competence-modal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var id = button.data('id') // Extract info from data-* attributes
    var modal = $(this)
    modal.find('form').attr('action', '/competences/' + id);
  });

  $('#competence-clone-modal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var id = button.data('id') // Extract info from data-* attributes
    var name = button.data('title')
    var modal = $(this)
    modal.find('.title').val('copia de ' + name);
    modal.find('#id').val(id);
  });
  
})

</script>
@endsection

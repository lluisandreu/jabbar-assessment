<div id="import-vue">
  <div id="excel-modal" class="modal fade in modal-primary" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Importación de perfiles</h4>
        </div>
        <div class="modal-body">
          <div class="alert alert-danger" v-if="errors.length" v-for="error in errors">
            <p>@{{ error }}</p>
          </div>
          <form enctype="multipart/form-data" id="profile-import-form" action="" method="post">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group ">
                  <label class="control-label">Selecciona una empresa</label>
                  <select name="company" id="company" class="form-control" data-validation="required" v-model="company" v-on:change="loadProject(company)">
                    @foreach(App\Company::all() as $company)
                      <option value="{{ $company->id }}" @if(old('company') && old('company') == $company->id) selected @endif>{{ $company->name }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label">Proyecto</label>
                  <select name="project" id="project" class="form-control" data-validation="required" v-model="project" :disabled="projectOptions.length == 0">
                    <option v-for="option in projectOptions" :value="option.id">@{{ option.name }}</option>
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Importa el archivo</label>
                  <input type="file" id="file" ref="file" class="form-control" data-validation="required" accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" v-on:change="loadFile()">
                  <p class="help-block">Formatos Excel .xlsx o .xls</p>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal" v-on:click="reset()">Cancela</button>
          <button type="submit" v-on:click="submitForm()" class="btn btn-success" :disabled="checkImport()">Empieza la importación</button>
        </div>
      </div>
    </div>
  </div>
  <div id="import-modal" class="modal fade in modal-primary" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Define los campos del perfil con las columnas del documento
            <span class="pull-right"><strong>@{{ rows.length }} Filas</strong></span>
          </h4>
        </div>
        <div class="modal-body">
          <div class="panel panel-danger" v-if="Object.keys(errors).length">
            <div class="panel-heading" id="import-errors" role="tab">
              <a class="panel-title collapsed" data-toggle="collapse" href="#import-errors-panel" data-parent="#exampleAccordionDefault" aria-expanded="false" aria-controls="exampleCollapseDefaultOne">
              Hay @{{ Object.keys(errors).length }} errores
            </a>
            </div>
            <div class="panel-collapse collapse" id="import-errors-panel" aria-labelledby="import-errors-panel" role="tabpanel" style="">
              <div class="panel-body">
                <ul>
                  <li v-for="error in errors">@{{ error }}</li>
                </ul>
              </div>
            </div>
          </div>
          <div class="alert alert-success" v-if="valid">
            <p>@{{ validMessage }}</p>
          </div>
          <form>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group ">
                  <label>¿Por qué fila empezamos a importar?</label>
                  <select name="row_start" class="form-control" v-model="rowStart">
                    <option :value="index" v-for="(row, index) in rows">Fila @{{ parseInt(index) + 1}}, @{{ row }} </option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="name">Nombre</label>
                  <select id="name" name="name" class="form-control" v-model="mapping.name">
                    <option :value="index" v-for="(col, index) in rows[rowStart]">@{{ parseInt(index) + 1}}, @{{ col }} </option>
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group ">
                  <label for="first_surname">Apellido </label>
                  <select id="first_surname" v-model="mapping.first_surname" class="form-control">
                    <option :value="index" v-for="(col, index) in rows[rowStart]">@{{ parseInt(index) + 1}}, @{{ col }} </option>
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group ">
                  <label for="second_surname">Segundo apellido</label>
                  <select id="second_surname" v-model="mapping.second_surname" class="form-control">
                    <option :value="index" v-for="(col, index) in rows[rowStart]">@{{ parseInt(index) + 1}}, @{{ col }} </option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group ">
                  <label for="birthdate">Fecha de nacimiento</label>
                  <select id="birthdate" v-model="mapping.birthdate" class="form-control">
                    <option :value="index" v-for="(col, index) in rows[rowStart]">@{{ parseInt(index) + 1}}, @{{ col }} </option>
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group ">
                  <label for="telephone">Teléfono </label>
                  <select id="telephone" v-model="mapping.telephone" class="form-control">
                    <option :value="index" v-for="(col, index) in rows[rowStart]">@{{ parseInt(index) + 1}}, @{{ col }} </option>
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group ">
                  <label for="email">Correo electrónico</label>
                  <select id="email" v-model="mapping.email" class="form-control">
                    <option :value="index" v-for="(col, index) in rows[rowStart]">@{{ parseInt(index) + 1}}, @{{ col }} </option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group ">
                  <label for="province">Provincia</label>
                  <select id="province" v-model="mapping.province" class="form-control">
                    <option :value="index" v-for="(col, index) in rows[rowStart]">@{{ parseInt(index) + 1}}, @{{ col }} </option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group ">
                  <label for="city">Ciudad </label>
                  <select id="city" v-model="mapping.city" class="form-control">
                    <option :value="index" v-for="(col, index) in rows[rowStart]">@{{ parseInt(index) + 1}}, @{{ col }} </option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group ">
                  <label for="position">Fecha de evaluación</label>
                  <select id="position" v-model="mapping.evaluation_date" class="form-control">
                    <option value="99">Importa con la fecha actual</option>
                    <option :value="index" v-for="(col, index) in rows[rowStart]">@{{ parseInt(index) + 1}}, @{{ col }} </option>
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group ">
                  <label for="position">Posición en la empresa</label>
                  <select id="position" v-model="mapping.position" class="form-control">
                    <option :value="index" v-for="(col, index) in rows[rowStart]">@{{ parseInt(index) + 1}}, @{{ col }} </option>
                  </select>
                  <p class="help-block">Aségurate que las posiciones del archivo coinciden con las de tu empresa</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group ">
                  <label for="function">Función en la empresa </label>
                  <select id="function" v-model="mapping.function" class="form-control">
                    <option :value="index" v-for="(col, index) in rows[rowStart]">@{{ parseInt(index) + 1}}, @{{ col }} </option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group ">
                  <label for="linkedin">Linkedin</label>
                  <select id="linkedin" v-model="mapping.linkedin" class="form-control">
                    <option value="">Ignora este campo</option>
                    <option :value="index" v-for="(col, index) in rows[rowStart]">@{{ parseInt(index) + 1}}, @{{ col }} </option>
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group ">
                  <label for="facebook">Facebook </label>
                  <select id="facebook" v-model="mapping.facebook" class="form-control">
                    <option value="">Ignora este campo</option>
                    <option :value="index" v-for="(col, index) in rows[rowStart]">@{{ parseInt(index) + 1}}, @{{ col }} </option>
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group ">
                  <label for="twitter">Twitter</label>
                  <select id="twitter" v-model="mapping.twitter" class="form-control">
                    <option value="">Ignora este campo</option>
                    <option :value="index" v-for="(col, index) in rows[rowStart]">@{{ parseInt(index) + 1}}, @{{ col }} </option>
                  </select>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" v-on:click="verify()" class="btn btn-warning">Verifica</button>
          <button type="button" v-on:click="store()" class="btn btn-success" :disabled="!valid" v-on:click="store()">Importa</button>
        </div>
      </div>
    </div>
  </div>
</div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form enctype="multipart/form-data" id="profile-form" action="{{ url('profiles') }}@isset($profile)/{{ $profile->id }}@endisset" method="POST" accept-charset="utf-8">
	@isset($profile)
		{{ method_field('PUT') }}
		@endisset
	{{ csrf_field() }}
	<div class="row">
		<div class="col-md-12 col-lg-6">
			<div class="form-group ">
				<label class="label-control sr-only">Nombre</label>
				<input type="text" class="form-control" placeholder="Nombre" id="name" name="name" value="{{ old('name', isset($profile->name)?$profile->name:'') }}" data-validation="required">
			</div>
			<div class="form-group ">
				<div class="row">
					<div class="col-sm-6">
						<label class="control-label sr-only">Primer apellido</label>
						<input type="text" class="form-control" placeholder="Primer apellido" id="first_surname" name="first_surname" value="{{ old('first_surname', isset($profile->first_surname)?$profile->first_surname:'') }}" data-validation="required">
						
					</div>
					<div class="col-sm-6">
						<label class="control-label sr-only">Segundo apellido</label>
						<input type="text" class="form-control" placeholder="Segundo apellido" id="second_surname" name="second_surname" value="{{ old('second_surname', isset($profile->second_surname)?$profile->second_surname:'') }}">
					</div>
				</div>
			</div>
			<div class="form-group ">
				<label class="label-control">Género</label>
				<div class="input-group">
					<div class="radio-custom radio-primary">
	                  <input type="radio" id="female" name="gender" value="1" @if(old('gender')) @if(old('gender') == 1) checked @endif @endif @isset($profile->gender) @if($profile->gender == 1) checked @endif @endisset>
	                  <label for="female">Mujer</label>
	                </div>
					<div class="radio-custom radio-primary">
	                  <input type="radio" id="male" name="gender" value="2" @if(old('gender')) @if(old('gender') == 2) checked @endif @endif @isset($profile->gender) @if($profile->gender == 2) checked @endif @endisset>
	                  <label for="female">Hombre</label>
	                </div>
				</div>
			</div>
			<div class="form-group ">
				<label class="label-control">Fecha de nacimiento</label>
				<div class="input-group">
					<span class="input-group-addon">
	                	<i class="fa fa-calendar" aria-hidden="true"></i>
	                </span>
					<input type="date" name="birthdate" class="form-control datepicker" data-plugin="datepicker" value="{{ old('birthdate', isset($profile->birthdate)?$profile->birthdate:'') }}" data-validation="date required" data-validation-format="yyyy-mm-dd">
				</div>
			</div>
			<div class="form-group ">
				<div class="row">
					<div class="col-md-6">
						<div class="input-group">
							<span class="input-group-addon">
			                	<i class="fa fa-phone" aria-hidden="true"></i>
			                </span>
							<input type="text" class="form-control" name="telephone" id="telephone" placeholder="Teléfono" value="{{ old('telephone', isset($profile->telephone)?$profile->telephone:'') }}">
						</div>
					</div>
					<div class="col-md-6">
						<div class="input-group">
							<span class="input-group-addon">
			                	<i class="fa fa-envelope" aria-hidden="true"></i>
			                </span>
							<input type="email" class="form-control" name="email" id="email" placeholder="Email" value="{{ old('email', isset($profile->email)?$profile->email:'') }}"  data-validation="email required">
						</div>
					</div>
				</div>
			</div>
			<div class="form-group ">
				<div class="row">
					<div class="col-md-6">
						<label class="control-label">Provincia</label>
						<select class="form-control" name="province" id="province" data-validation="required">
							<option hidden disabled selected>Selecciona una provincia</option>
							@foreach(App\Province::all() as $provincia)
								<option value="{{ $provincia->id }} " @if(isset($profile->province) && $profile->province == $provincia->id) selected @endif @if(old('province') && old('province') == $provincia->id) selected @endif>{{ $provincia->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-6">
						<label class="control-label">Ciudad</label>
						<input type="text" class="form-control" name="city" id="city" placeholder="Ciudad" value="{{ old('city', isset($profile->city)?$profile->city:'') }}" data-validation="required">
					</div>
				</div>
			</div>
			<div class="form-group ">
				<label class="control-label">Empresa</label>
				<select name="company" id="company" class="form-control" data-validation="required">
					<option hidden selected disabled>Selecciona una empresa</option>
					@foreach(App\Company::all() as $company)
						<option value="{{ $company->id }}" @isset($profile->company_id) @if($profile->company_id == $company->id) selected @endif @endisset @if(old('company') && old('company') == $company->id) selected @endif>{{ $company->name }}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group ">
				<label class="control-label">Posición en la empresa</label>
				<select name="position" id="position" class="form-control" @isset($profile) readonly @endisset data-validation="required">
					<option hidden selected disabled>Selecciona una posición en la empresa</option>
					@isset($profile->company->positions)
						@foreach($profile->company->positions as $position)
							<option value="{{ $position->id }}" @if($profile->position_id == $position->id) selected @endif>{{ $position->name }}</option>
						@endforeach
					@endisset
				</select>
			</div>
			<div class="form-group">
				<label class="control-label">Proyecto</label>
				<select name="project" id="project" class="form-control" @isset($profile) readonly @endisset data-validation="required">
					<option hidden selected disabled>Selecciona el proyecto</option>
					@isset($profile->company->projects)
						@foreach($profile->company->projects as $project)
							<option value="{{ $project->id }}" @if($profile->project_id == $project->id) selected @endif>{{ $project->name }}</option>
						@endforeach
					@endisset
				</select>
			</div>
			<div class="form-group ">
				<label class="control-label sr-only">Función</label>
				<input type="text" class="form-control" name="function" id="function" placeholder="Función" value="{{ old('function', isset($profile->function)?$profile->function:'') }}" data-validation="required">
			</div>
			<div class="form-group ">
				<label class="label-control">Fecha de evaluación</label>
				<div class="input-group">
					<span class="input-group-addon">
	                	<i class="fa fa-calendar" aria-hidden="true"></i>
	                </span>
					<input type="date" name="evaluation_date" id="evaluation_date" class="form-control datepicker" data-plugin="datepicker" value="{{ old('evaluation_date', isset($profile->evaluation_date)?$profile->evaluation_date:'') }}" data-validation="date">
				</div>
			</div>
			<div class="form-group ">
				<button type="submit" class="btn btn-primary">@isset($profile)Actualizar @else Crear perfil @endisset</button>
				@isset($profile) 
					<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-{{$profile->id}}">Elimina</button>
				@endisset
			</div>
	    </div>
	    <div class="col-md-12 col-lg-6">
	    	<div class="form-group">
		    	<label for="picture" class="control-label">Consultor</label>
		    	<select name="consultor" id="consultor" class="form-control" data-validation="required">
					<option hidden selected disabled>Selecciona el consultor asociado</option>
					@foreach(App\User::all() as $user)
						<option value="{{ $user->id }}" @if($profile->user_id == $user->id) selected @endif>{{ $user->name }}</option>
					@endforeach
				</select>
	    	</div>
	    	<div class="form-group">
		    	<label for="picture" class="control-label">Fotografía</label>
		    	@isset($profile->picture)
		    		<img src="{{  asset('storage/profiles/' . $profile->picture) }}">
		    	@endisset
		    	<input type="file" class="dropify form-control" id="picture" name="picture" data-plugin="dropify" data-height="300">
	    	</div>
	    	<div class="form-group">
	    		<label for="cv" class="control-label">CV</label>
		    	@isset($profile->cv)
		    		<a href="{{  asset('storage/cv/' . $profile->cv) }}"><i class="fa fa-file-o"></i> {{ $profile->name}} CV</a>
		    	@endisset
		    	<input type="file" class="form-control" id="cv" name="cv">
	    	</div>
	    	<div class="form-group ">
				<label class="control-label sr-only">Linkedin</label>
				<div class="input-group">
	                <span class="input-group-addon">
	                  <i class="fa fa-linkedin" aria-hidden="true"></i>
	                </span>
	                <input type="text" placeholder="Linkedin" name="linkedin" class="form-control" id="linkedin" value="{{ old('linkedin', isset($profile->linkedin)?$profile->linkedin:'') }}">
	          	</div>
	        </div>
	        <div class="form-group ">
				<label class="control-label sr-only">Facebook</label>
				<div class="input-group">
	                <span class="input-group-addon">
	                  <i class="fa fa-facebook" aria-hidden="true"></i>
	                </span>
	                <input type="text" placeholder="Facebook" name="facebook" class="form-control" id="facebook" value="{{ old('facebook', isset($profile->facebook)?$profile->facebook:'') }}">
	          	</div>
	        </div>
	        <div class="form-group ">
				<label class="control-label sr-only">Twitter</label>
				<div class="input-group">
	                <span class="input-group-addon">
	                  <i class="fa fa-twitter" aria-hidden="true"></i>
	                </span>
	                <input type="text" placeholder="Twitter" name="twitter" class="form-control" id="twitter" value="{{ old('twitter', isset($profile->twitter)?$profile->twitter:'') }}">
	          	</div>
	        </div>
	    </div>
	</div>
</form>
@isset($profile)
<div id="modal-{{$profile->id}}" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Realmente quieres eliminar este perfil?</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <form action="/profiles/{{$profile->id}}" method="post" class="pull-right">
         {{ method_field('DELETE') }}
         {{ csrf_field() }}
         <button type="submit" class="btn btn-danger">Elimina</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endisset

@section('inline-scripts')
	<script>

		$(document).ready(function() {

			$.validate({
				form: '#profile-form',
				addValidClassOnAll : true,
			    lang: 'es',
			    modules : 'html5, date',
			});

			$('.datepicker').datepicker({
				format: 'yyyy-mm-dd',
			});

			var company = $('#company');

			company.on('click change', function(event) {
				$('#position').html('');
				$('#project').html('');

				event.preventDefault();

				id = $(this).val();

				axios.get('/positions/json', {
					params: {
						id: id
					}
				})
				.then(function (response) {
				   //console.log(response);
				   $('#position').removeAttr("readonly");
				   for (var i = response.data.length - 1; i >= 0; i--) {
				   	$('#position').append('<option value="' + response.data[i].id + '">' + response.data[i].name + '</option>');
				   };
				})
				.catch(function (error) {
				  console.log(error);
				});

				axios.get('/projects/json', {
					params: {
						id: id
					}
				})
				.then(function (response) {
				   //console.log(response);
				   $('#project').removeAttr("readonly");
				   for (var i = response.data.length - 1; i >= 0; i--) {
				   	$('#project').append('<option value="' + response.data[i].id + '">' + response.data[i].name + '</option>');
				   };
				})
				.catch(function (error) {
				  console.log(error);
				});
			});

			@if(old('position'))
				$('#position').html('');
				$('#position').append('<option value="' + '{!! old('position') !!}' + '">' + '{{ App\CompanyPosition::find(old('position'))->name }}' + '</option>');
			@endif

			@if(old('project'))
				$('#project').html('');
				$('#project').append('<option value="' + '{!! old('project') !!}' + '">' + '{{ App\Project::find(old('project'))->name }}' + '</option>');
			@endif
		});
	</script>
@endsection
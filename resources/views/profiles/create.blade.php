@extends('layouts.assessment')
@section('content')
<div class="page animsition">
  <div class="page-header">
    @isset($profile)
      <h1 class="page-title">Edita {{ $profile->name }}</h1>
      @isset($profile->company) {{ $profile->company->name }} @endisset
    @else 
      <h1 class="page-title">Crea un nuevo perfil</h1>
    @endisset
  </div>
  <div class="page-content">
    <div class="panel">
      <div class="panel-body container-fluid">
        @include('profiles.form')
        @isset($profile)
          <a href="{{ route('profile_form', ['id' => $profile->id]) }}" class="btn btn-secondary">Accede al formulario</a>
        @endisset
      </div>
    </div>  
  </div>
</div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/locales/bootstrap-datepicker.es.min.js"></script>
@endsection
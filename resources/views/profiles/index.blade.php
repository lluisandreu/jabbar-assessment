@extends('layouts.assessment')

@section('content')
  <div class="page">
    <h1 class="page-header">Todos los perfiles</h1>
    @if (count($errors) > 0)
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
    @endif
    <div class="container-fluid">
      <div class="row row-lg">
        <div class="col-xs-12 col-xl-12">
          <div class="profiles-panel panel panel-default">
            <div class="panel-body">
              <table id="profiles-table"></table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <a class="site-action btn-raised btn btn-success btn-floating" href="{{ url('profiles/create') }}">
      <i class="fa fa-plus" aria-hidden="true"></i>
    </a>
    <a class="site-action btn-raised btn btn-success btn-floating" style="bottom:130px" href="{{ url('profiles/create') }}" data-toggle="modal" data-target="#excel-modal">
      <i class="far fa-file-excel" aria-hidden="true"></i>
    </a>
  </div>
@endsection

@section('modals')
<div id="profile-modal" class="modal fade in" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-simple" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Realmente quieres eliminar este perfil?</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <form action="#" method="post" class="pull-right">
         {{ method_field('DELETE') }}
         {{ csrf_field() }}
         <button type="submit" class="btn btn-danger">Elimina</button>
        </form>
      </div>
    </div>
  </div>
</div>
@include('partials.profile-importer')
@endsection

@section('inline-scripts')

<script>
  $(document).ready(function() {
    $('#profiles-table').bootstrapTable({
      url: '/profiles/json',
      search: true,
      showColumns: true,
      hideUnusedSelectOptions: true,
      alignmentSelectControlOptions: 'auto',
      pagination: true,
      filterControl: true,
      idField: 'id',
      uniqueId: 'id',
      sortName: 'name',
      sortOrder: 'asc',
      classes: 'table table-hover table-striped table-no-bordered',
      columns: [{
        field: 'id',
        title: 'Id',
        sortable: true,
        filterControl: 'input'
      },{
        field: 'picture',
        title: 'Fotografía',
        formatter: pictureFormatter
      },{
        field: 'name',
        title: 'Nombre',
        sortable: true,
        filterControl: 'input'
      },{
        field: 'email',
        title: 'Email',
        sortable: true,
        filterControl: 'input'
      },{
        field: 'company',
        title: 'Empresa',
        sortable: true,
        filterControl: 'select'
      },{
        field: 'position',
        title: 'Posición',
        sortable: true,
        filterControl: 'select'
      },{
        field: 'project',
        title: 'Proyecto',
        sortable: true,
        filterControl: 'select',
      },{
        field: 'formstatus',
        title: 'Completado',
        filterControl: 'input'
      },{
        field: 'id',
        title: 'Acciones',
        formatter: actionsFormatter,
      },
      ],


    });

    $('.no-filter-control').css('height', '0');

    function actionsFormatter(value, row, index, field) {
      var options = '';
      options += '<a href="profiles/' + value + '/form" class="btn"><i class="fa fa-question"></i></a>';
      options += '<a href="profiles/' + value + '/edit" class="btn"><i class="fa fa-edit"></i></a>';
      options += '<button type="button" class="btn" data-toggle="modal" data-target="#profile-modal" data-id="'+value+'"><i class="fa fa-trash"></i></button>'
      return options;
    }

    function pictureFormatter(value) {
      if(typeof value != 'undefined') {
        return '<a class="avatar"><img src="'+ value +'" alt=""></a>';
      }
      
    }
  });
</script>

<script>
$(document).ready(function() {
  $('#profile-modal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var id = button.data('id') // Extract info from data-* attributes
    var modal = $(this)
    modal.find('form').attr('action', '/profiles/' + id);
  });
  $.validate({
    form: '#profile-import-form',
    addValidClassOnAll : true,
      lang: 'es',
      modules : 'html5, file',
  });
});

var importer = new Vue({
  el: '#import-vue',
  data: {
    company: null,
    projectOptions: [],
    project: null,
    rowStart: 0,
    file: null,
    rows: [],
    errors: [],
    mapping: {
      name: 0,
      first_surname: 1,
      second_surname: 2,
      birthdate: 3,
      telephone: 4,
      email: 5,
      province: 6,
      city: 7,
      position: 8,
      evaluation_date: 99,
      function: 9,
      linkedin: '',
      facebook: '',
      twitter: '',
    },
    valid: false,
    validMessage: ''
  },
  methods: {
    loadProject: function(company) {
      var t = this;
      axios.get('/projects/json', {
        params: {
          id: company
        }
      })
      .then(function (response) {
         //console.log(response.data);
         if(response.data != '') {
          t.projectOptions = response.data;
          t.project = 1;
         } else {
          t.projectOptions = [];
         }
      })
      .catch(function (error) {
        //console.log(error);
      });
    },
    loadFile: function() {
      this.file = this.$refs.file.files[0];
    },
    checkImport: function() {
      if(!this.company || !this.project || !this.file) {
        return true;
      } else {
        return false;
      }
    },
    submitForm: function() {
      var formData = new FormData();
      var t = this;
      formData.append('file', this.file);
      formData.append('company', this.company);
      formData.append('project', this.project);
      axios.post('/profiles/import', 
        formData,
        {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        }
      ).then(function(response) {
        if(response.data != '') {
          t.rows = response.data;
        } else {
          t.errors.push('No hemos encontrado ninguna columna en el documento');
          t.rows = [];
        }
        $('#excel-modal').modal('hide');
        $('#import-modal').modal('show');
      })
      .catch(function(error){
        t.errors.push('Ha habido un error con el documento');
      });
    },
    verify: function() {
      var formData = new FormData();
      var t = this;
      formData.append('rowStart', this.rowStart);
      formData.append('mapping', JSON.stringify(this.mapping));
      formData.append('company', this.company);
      formData.append('project', this.project);

      axios.post('/profiles/import/verify', 
        formData
      ).then(function(response) {
        if(response.data == 'valid') {
          t.valid = true;
          t.validMessage = "No hay errores de validación, se pueden importar los perfiles";
          t.errors = [];
        }
      })
      .catch(function(error){
        t.errors = error.response.data;
        t.valid = false;
      });
    },
    store: function() {
      var formData = new FormData();
      var t = this;
      formData.append('rowStart', this.rowStart);
      formData.append('mapping', JSON.stringify(this.mapping));
      formData.append('company', this.company);
      formData.append('project', this.project);

      console.log(JSON.stringify(this.mapping));
      axios.post('/profiles/import/store', 
        formData
      ).then(function(response) {
        if(response.data == 'imported') {
          window.location = '/profiles/import/success';
        }
      })
      .catch(function(error){
        t.errors = error.response.data;
      });
    },
  }
});

$('#import-modal').on('hidden.bs.modal', function(event) {
  event.preventDefault();
  importer.$data.rows = [];
  //importer.$data.mapping = [];
  importer.$data.errors = [];
  importer.$data.valid = false;
});
  

</script>
@endsection


 

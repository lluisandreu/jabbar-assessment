@extends('layouts.assessment')
@section('content')
<div class="page animsition">
  <div class="page-header">
      <h1 class="page-title">Configura la importación {{ $profile->name }}</h1>
  </div>
  <div class="page-content">
    <div class="panel">
      <div class="panel-body container-fluid">
        @include('profiles.form')
        @isset($profile)
          <a href="{{ route('profile_form', ['id' => $profile->id]) }}" class="btn btn-secondary">Accede al formulario</a>
        @endisset
      </div>
    </div>  
  </div>
</div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/locales/bootstrap-datepicker.es.min.js"></script>
@endsection
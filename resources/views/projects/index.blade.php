@extends('layouts.assessment')

@section('content')
  <div class="page">
    <h1 class="page-header">Todos los proyectos</h1>
    @if (count($errors) > 0)
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
    @endif
    <div class="container-fluid">
      <div class="row row-lg">
        <div class="col-xs-12 col-xl-12">
          <div class="profiles-panel panel panel-default">
            <div class="panel-body">
              <table class="table table-hover table-striped">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Empresa</th>
                    <th>
                  </tr>
                </thead>
                {{-- Not loading with ajax for now --}}
                <tbody>
                  @foreach($projects as $project)
                    <tr>
                      <td>{{ $project->id }}</td>
                      <td>{{ $project->name }}</td>
                      <td>@isset($project->company) {{ $project->company->name }} @else <span class="badge">Sin asignar empresa</span> @endisset</td>
                      <td class="text-right">
                        <a href="{{ url('projects/'.$project->id.'/edit') }}" class="btn"><i class="icon wb-pencil"></i></a>
                        <button type="button" id="rm-modal" class="btn" data-toggle="modal" data-target="#project-modal" data-id="{{$project->id}}"><i class="fa fa-trash"></i></button>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <a class="site-action btn-raised btn btn-success btn-floating" href="{{ url('projects/create') }}">
      <i class="icon wb-plus" aria-hidden="true"></i>
    </a>
  </div>
@endsection

@section('modals')
<div id="project-modal" class="modal fade in" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-simple" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Realmente quieres eliminar este proyecto?</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <form action="#" method="post" class="pull-right">
         {{ method_field('DELETE') }}
         {{ csrf_field() }}
         <button type="submit" class="btn btn-danger">Elimina</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

@section('inline-scripts')
<script>
  $(document).ready(function() {
    $('#project-modal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var id = button.data('id') // Extract info from data-* attributes
    var modal = $(this)
    modal.find('form').attr('action', '/projects/' + id);
  });
  
})

</script>
@endsection

@push('styles')
  <link rel="stylesheet" type="text/css" href="/vendor/remark/global/vendor/bootstrap-select/bootstrap-select.min.css">
@endpush

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form id="project-form" enctype="multipart/form-data" action="{{ url('projects') }}@isset($project)/{{ $project->id }}@endisset" method="POST" accept-charset="utf-8">
	@isset($project)
		{{ method_field('PUT') }}
		@endisset
	{{ csrf_field() }}
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="form-group col-md-4">
					<label class="label-control">Nombre</label>
					<input type="text" class="form-control" placeholder="Nombre del proyecto" id="name" name="name" value="{{ old('name', isset($project->name)?$project->name:'') }}" data-validation="required">
				</div>
				<div class="form-group col-md-4">
					<label class="label-control">Empresa</label>
					<select class="form-control" id="company" name="company_id" data-validation="required">
						<option value="" hidden selected disabled>Selecciona una empresa</option>
						@foreach(App\Company::all() as $company)
							<option value="{{ $company->id}}" @isset($project->company_id) @if($project->company_id == $company->id) selected @endif @endisset>{{ $company->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Diccionario de competencias</label>
						<select class="form-control" id="dictionary" name="dictionaries[]"  data-validation="required"> 
							@isset($project->company)
								@foreach($project->company->dictionaries as $dictionary)
									<option value="{{ $dictionary->id }}" @if($project->dictionaries->pluck('id')->contains($dictionary->id)) selected @endif>{{ $dictionary->name }}</option>
								@endforeach
							@endisset
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-8">
					<button type="submit" id="form-submit" class="btn btn-primary">@isset($project)Actualiza @else Crea @endisset</button>
				</div>
			</div>
		</div>
	</div>
</form>

@section('scripts')
<script src="/vendor/remark/global/vendor/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
@endsection

@section('inline-scripts')
	<script>
		$(document).ready(function() {

			$.validate({
				form: '#project-form',
				addValidClassOnAll : true,
			    lang: 'es',
			    modules : 'html5, date, security, location',
			    onModulesLoaded : function() {
		 
			      // Show strength of password
			      //$('input[name="password"]').displayPasswordStrength();
			     }
			});


			var company = $('#company');
			var select = $('.selectpicker');
	    	select.selectpicker({
	      		enableFiltering: true
	    	});

			company.on('change', function(event) {
				event.preventDefault();
				$('#dictionary').html('');
				id = $(this).val();
				axios.get('/dictionaries/index/json', {
					params: {
						id: id
					}
				})
				.then(function (response) {
				   //console.log(response);
				   $('#dictionary').removeAttr("disabled");
				   if(response.data != '') {
				   	for (var i = response.data.length - 1; i >= 0; i--) {
				   		$('#dictionary').append('<option value="' + response.data[i].id + '">' + response.data[i].name + '</option>');
				   		select.selectpicker('refresh');
				   	}
				   } else {
				   	$('#dictionary').html('');
				   	select.selectpicker('refresh');
				   }
				   
				})
				.catch(function (error) {
				  console.log(error);
				});
			});
			
		});
	</script>
@endsection
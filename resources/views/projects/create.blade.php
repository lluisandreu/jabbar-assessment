@extends('layouts.assessment')
@section('content')
<div class="page animsition">
  <div class="page-header">
    @isset($project)
      <h1 class="page-title">Edita {{ $project->name }}</h1>
    @else 
      <h1 class="page-title">Crea un nuevo proyecto</h1>
    @endisset
  </div>
  <div class="page-content container-fluid">
    <div class="panel">
      <div class="panel-body">
        @include('projects.form')
      </div>
    </div>  
  </div>
</div>
@endsection
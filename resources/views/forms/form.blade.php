@extends('layouts.assessment')

@section('content')
  <div class="page" id="vue-form">
    <h1 class="page-header">Formulario de {{ $profile->name }}</h1>
    @if (count($errors) > 0)
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
    @endif
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
      <li class="breadcrumb-item"><a href="{{ url('profiles') }}">Perfiles</a></li>
      <li class="breadcrumb-item active"><a href="#"></a> Cuestionario</li>
    </ol>
    <div class="container-fluid">
      <div class="row row-lg">
        <div class="col-xs-12 col-xl-12">
          <div class="form-panel panel panel-default">
            <div class="panel-header">
              <h2 class="panel-title">Selecciona un indicador para rellenar el formulario</h2>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-12 col-lg-6">
                  <div class="panel-group panel-group-continuous" id="areas" aria-multiselectable="true" role="tablist">
                    @foreach($areas as $area)
                      <div class="panel panel-primary">
                        <div class="panel-heading" id="" role="tab">
                        <a class="panel-title collapsed" data-parent="#areas" data-toggle="collapse" href="#{{ strtolower($area->name) }}" aria-controls="{{ strtolower($area->name) }}" aria-expanded="false">
                          {{ $area->name }}
                        </a>
                        </div>
                        <div class="panel-collapse collapse" id="{{ strtolower($area->name) }}" role="tabpanel" style="">
                        <div class="panel-body">
                          @foreach($area->indicators as $indicator)
                            <div class="row">
                              <div class="col-xs-10"><a href="#" data-name="{{ $indicator->name }}" v-on:click.prevent="load({!! $indicator->id !!}, $event)">{{ $indicator->name }}</a></div>
                              <div class="col-xs-2 text-right"><span data-indicator-submissions="{!! $indicator->id !!}" v-html="countIndicatorSubmissions({!! $indicator->id !!})"></span> / {{ count($indicator->evidences) + count($indicator->profileCompetences($profile->id)) }}</div>
                            </div>
                          @endforeach
                        </div>
                      </div>
                    </div>
                    @endforeach
                  </div>
                </div>
                <div class="col-md-6 col-lg-3">
                  <div class="panel panel-primary panel-line">
                    <div class="panel-heading">
                      <h3 class="panel-title">{{ $profile->name }}</h3>
                    </div>
                    <div class="panel-body">
                      <p>Empresa: {{ $profile->company->name }}</p>
                      <p>Proyecto: {{ $profile->project->name }}</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-3">
                  <div class="panel panel-primary panel-line">
                    <div class="panel-heading">
                      <h3 class="panel-title">Evidencias</h3>
                    </div>
                    <div class="panel-body">
                      <div class="progress progress-lg">
                        <div class="progress-bar progress-bar-danger" :style="{ width: progressBarVar + '%' }" role="progressbar">@{{ countSubmissions }} / @{{ totalQuestions }}</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="form-panel panel panel-default" v-if="evidences || competences">
            <div class="panel-header">
              <h2 class="panel-title">@{{ formTitle }}</h2>
            </div>
            <div class="panel-body">
              <div class="evidencias panel panel-primary panel-line" v-if="evidences">
                <div class="panel-heading">
                  <h3 class="panel-title">Rellena todas las evidencias</h3>
                </div>
                <div class="panel-body">
                  <ul class="list-group list-group-bordered">
                    <li class="list-group-item clearfix" v-for="(evidence, ekey) in evidences">
                      <div class="row">
                        <div class="col-xs-9">@{{ evidence.body }}</div>
                        <div class="col-xs-3 text-right">
                          <div class="btn-group" data-toggle="buttons" role="group">
                            <label class="btn btn-outline btn-primary" v-for="(value, key) in evidence.valuations" v-on:click="select(evidence.id, 'evidence', value, $event)" :class="{ 'btn-success': checkValue(evidence.id,'evidence',value) }">
                              <input type="radio" :name="evidence.id" :id="key" :value="value" :checked="checkValue(evidence.id,'evidence',value)"> @{{ key }}
                            </label>
                          </div>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="competencias panel panel-primary panel-line" v-if="competences">
                <div class="panel-heading">
                  <h3 class="panel-title">Rellena todas las competencias</h3>
                </div>
                <div class="panel-body">
                  <ul class="list-group list-group-bordered">
                    <li class="list-group-item clearfix" v-for="(competence, ekey) in competences">
                      <div class="row">
                        <div class="col-xs-9">@{{ competence.description }}</div>
                        <div class="col-xs-3 text-right">
                          <div class="btn-group" data-toggle="buttons" role="group">
                            <label class="btn btn-outline btn-primary" v-for="(value, key) in competence.levels" v-on:click="select(competence.id, 'competence', value.level, $event)" :class="{ 'btn-success': checkValue(competence.id, 'competence', value.level) }">
                              <input type="radio" :name="competence.id" :id="key" :value="value.level" :checked="checkValue(competence.id, 'competence', value.level)"> @{{ value.level }}
                            </label>
                          </div>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('inline-scripts')
<script>
    
    var vue = new Vue({
      el: '#vue-form',
      data: {
        indicator: '',
        formTitle: '',
        evidences: '',
        competences: '',
        allCompetences: {!! count($competences) !!},
        allEvidences: {!! count($evidences) !!},
        totalQuestions: 0,
        submissions: [],
        countSubmissions: 0,
        progressBarVar: 0,
      },
      created: function() {
        this.loadSubmissions();
        this.totalQuestions = this.allCompetences + this.allEvidences;
      },
      watch: {
        submissions: function() {
          this.progressBarVar = (this.submissions.length / this.totalQuestions) * 100;
          this.countSubmissions = this.submissions.length;
        }
      },
      methods: {
        load: function(id, event) {
          this.formTitle = event.target.dataset.name;
          this.indicator = id;
          this.evidences = '';
          this.competences = '';
          var t = this;
          // Load evidences
          axios.get('/evidences/json', {
            params: {
              id: id
            }
          }).then(function (response) {
            if(response.data.length > 0) {
              for (var i = response.data.length - 1; i >= 0; i--) {
                response.data[i].valuations = JSON.parse(response.data[i].valuations);
              };
              t.evidences = response.data;
            }
          })
          .catch(function (error) {
            console.log(error);
          });

          // Load competences
          axios.get('/profiles/competences', {
            params: {
              profile_id: {!! $profile->id !!},
              indicator_id: id
            }
          }).then(function (cresponse) {
            if(Object.keys(cresponse.data).length > 0) {
              t.competences = cresponse.data;
            }
           
          })
          .catch(function (error) {
            console.log(error);
          });

          console.log(this.competences.length);
        },
        select(id, type, value, event) {

          var el = event.target;
          $(el).siblings('.btn').removeClass('btn-success');
          var submission = [];
          if(type == 'evidence') {
            submission = {
              id: id,
              type: type,
              indicator_id: this.indicator,
              value: value
            };
          } else {
            submission = {
              id: id,
              type: type,
              indicator_id: this.indicator,
              value: value
            };
          }
          var t = this;
          axios({
            method: 'post',
            url: '/form/' + {!! $profile->id !!} + '/submission',
            data: submission
          }).then(function (response) {
            var exists = false;
            for (key in t.submissions) {
              if(t.submissions[key].id === id && t.submissions[key].type == type) {
                t.submissions[key].value = value;
                exists = true;
              }
            };
            if(!exists) {
              t.submissions.push(submission);
            }
            var currentSubmissions = $('*[data-indicator-submissions="' + submission.indicator_id + '"');
            currentSubmissions.html(1 + parseInt(currentSubmissions.html()));
            $(el).addClass('btn-success');
          })
          .catch(function (error) {
            console.log(error);
          });
          t.countIndicatorSubmissions(t.indicator);
        },
        loadSubmissions() {
          var t = this;
          axios.get('/submissions/json', {
            params: {
              profile_id: {!! $profile->id !!}
            },
          }).then(function (response) {
              console.log(response.data);
              for (key in response.data) {
                t.submissions.push(response.data[key]);
              };
              //t.progressBarVar = (t.submissions[0].length / t.allEvidences) * 100;
          })
          .catch(function (error) {
            console.log(error);
          });
          //t.progressBar(t.submissions, t.allEvidences);
        },
        checkValue(id, type, value) {
          for (key in this.submissions) {
            if(this.submissions[key].id === id && this.submissions[key].type === type) {
              if(this.submissions[key].value == value) {
                return true;
              } 
            }
          };
          return false;
        },
        getIndex(id, value) {
          for (var i = 0; i < this.submissions.length; i++) {
            if(this.submissions[i].id == id) {
              return i;
            }
          };
          return false;
        },
        countIndicatorSubmissions(id) {
          var c = 0;
          for (var i = 0; i < this.submissions.length; i++) {
            if(this.submissions[i].indicator == id) {
              c++;
            }
          };
          return c;
        },
        progressBar: function(submissions, evidences) {
          var percent = (submissions.length / totalQuestions) * 100;
          this.progressBarVar = percent;
        }
      },
    });

</script>
  
@endsection


@extends('layouts.assessment')
@push('styles')
  <link rel="stylesheet" type="text/css" href="/vendor/remark/global/vendor/nestable/nestable.min.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.webui-popover/1.2.1/jquery.webui-popover.min.css">
@endpush

@section('content')
<div class="page" id="position-vue">
  <h1 class="page-header">Áreas y puestos de la empresa {{ $company->name }}</h1>
  @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif
  <div class="container-fluid">
    <div class="row">
      <div class="col-xs-12 col-xl-12">
        <div class="panel panel-transparent">
          <div class="nav-tabs-horizontal nav-tabs-inverse" data-plugin="tabs">
              <ul class="nav nav-tabs nav-tabs-solid" role="tablist">
                <li class="nav-item" role="presentation">
                  <a class="nav-link active" data-toggle="tab" href="#puestotab" aria-controls="puestotab" role="tab">
                  <i class="fa fa-plus"></i> Puesto
                </a>
                </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="puestotab" role="tabpanel">
                  <form id="position-form" action="#" class="container-fluid form-horitzontal">
                    <div class="row">
                       <div class="col-md-3">
                         <div class="form-group row">
                          <label class="col-md-4 form-control-label text-right ">Nombre</label>
                          <div class="col-md-8">
                            <input type="text" class="form-control" name="name"  v-model="name" data-validation="required">
                          </div>
                         </div>
                       </div>
                       <div class="col-md-3">
                         <div class="form-group row">
                          <label class="col-md-3 form-control-label text-right ">Áreas</label>
                         <div class="col-md-9">
                           <select class="form-control" name="area" v-model="area" data-validation="required">
                             @foreach($areas as $area)
                              <option value="{{ $area->id }}">{{ $area->name }}</option>
                             @endforeach
                           </select>
                          </div>
                         </div>
                       </div>
                       <div class="col-md-3">
                         <div class="form-group row">
                          <label class="col-md-3 form-control-label text-right">Diccionario</label>
                         <div class="col-md-9">
                           <select class="form-control" name="dictionary" v-model="dictionary" data-validation="required" v-on:change="loadCompetences(dictionary)">
                             @foreach($company->dictionaries as $dict)
                              <option value="{{ $dict->id }}">{{ $dict->name }}</option>
                             @endforeach
                           </select>
                          </div>
                         </div>
                      </div>
                      <div class="col-md-2">
                          <button id="compentece-toggle" type="button" class="btn-outline btn btn-block btn-primary" :disabled="dictionary == ''"><i class="fa fa-plus"></i> Competencias</i></button>
                          <div class="webui-popover-content">
                            <div class="row" v-for="row in competences" v-if="fetchedCompetences.length">
                              <div class="col-sm-5">
                                <select class="form-control" v-model="row.competence" v-on:change="loadLevels(row.competence)"> 
                                  <option v-bind:value="option.id" v-if="competences.length" v-for="option in fetchedCompetences">
                                    @{{ option.description }}
                                  </option>
                                </select>
                              </div>
                              <div class="col-sm-5">
                                <select class="form-control" v-model="row.level">
                                  <option v-bind:value="option.id" v-for="option in row.lOptions">
                                    @{{ option.description }}
                                  </option>
                                </select>
                              </div>
                              <div class="col-sm-2">
                                <button type="button" class="btn btn-primary" v-on:click="addCompetence()"><i class="fa fa-plus"></i></button>
                              </div>
                            </div>
                          </div>
                      </div>
                      <div class="col-md-1">
                        <button type="button" class="btn btn-block btn-success" :disabled="area == '' || name == '' || competences[0].competence == '' || competences[0].level == ''" v-on:click="submitForm()"><span v-if="id == ''"><i class="fa fa-save"></i></span><span v-else><i class="fas fa-pencil-alt"></i></span></button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-xl-12">
        <div class="panel">
          <div class="panel-body">
            <div class="area-positions-nestable">
                @foreach($company->areas as $area)

                <div class="dd" data-plugin="nestable">
                  <ol class="dd-list">
                    <li class="dd-item dd-nodrag" data-type="area">
                      <div class="dd-nodrag">
                        <i class="fa fa-folder-open"></i> {{ $area->name }}
                      </div>
                      @isset($area->positions)
                        <ol class="dd-list">
                        @foreach($area->positions as $pos)
                            @if($pos->parent_id == 0)
                            <li class="dd-list" data-id="{{ $pos->id }}" data-type="position">
                                <div class="dd-handle">
                                  {{ $pos->name }} 
                                  @foreach($pos->competences as $comp)
                                    <div class="badge badge-primary"> 
                                      {{ $comp->name }} ({{ optional(\App\CompetenceLevel::find($comp->pivot->competence_level_id))->description }})
                                    </div> 
                                  @endforeach
                                  <span class="float-right">
                                    <a v-on:click="editMode('{{ $pos->id}}')"><i class="fa fa-edit"></i></a>
                                    {{-- <a class="open-edit-model" data-id="{{ $pos->id }}" data-name="{{ $pos->name }}" data-area="{{ $pos->company_area_id }}" data-competences="{{ $pos->getCompetences() }}"><i class="fa fa-edit"></i></a> --}}
                                    <a class="open-delete-model" data-id="{{ $pos->id }}"><i class="fa fa-trash"></i></a>
                                  </span>
                                </div>
                            @endif
                                @foreach($pos->children as $child)
                                  @include('companies.nestedpositions', ['child' => $child])
                                @endforeach
                             @if($pos->parent_id == 0)
                            </li>
                            @endif
                        @endforeach
                        </ol>
                      @endisset
                    </li>
                  </ol>
                  </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="delete-model" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Realmente quieres eliminar este puesto?</h4>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <form action="/positions/#id/delete" method="post" class="pull-right">
           {{ csrf_field() }}
           <button type="submit" class="btn btn-danger">Elimina</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="/vendor/remark/global/vendor/nestable/jquery.nestable.min.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/jquery.webui-popover/1.2.1/jquery.webui-popover.min.js"></script>
@endsection

@section('inline-scripts')

<script>

   var positionVue = new Vue({
      el: '#position-vue',
      data: {
        //editMode: false,
        company: '{!! $company->id !!}',
        id: '',
        name: '',
        area: '',
        dictionary: '',
        fetchedCompetences: '',
        competences: [{
          competence: '',
          level: '',
          lOptions: [],
        }],
      },
      mounted: function(){
        //this.competences.push({competence: '', level: ''})
      },
      watch: {
        level: function() {

        }
      },
      methods: {
        addCompetence: function() {
          this.competences.push({competence: '', level: '', lOptions: []})
        },
        loadCompetences: function(dict_id) {
          var t = this;
          t.competences = [{
            competence: '',
            level: '',
            lOptions: [],
          }];
          axios.get('/dictionary/competences/json', {
            params: {
              dict_id : dict_id
            }
          }).then(function (response) {
            if(response.data != '') {
              t.fetchedCompetences = response.data
            } else { 
              t.fetchedCompetences = [];
            }
          }).catch(function (error) {
            console.log(error);
          });
        },
        loadLevels: function(competence_id) {
          var t = this;
          axios.get('/levels/json', {
            params: {
              competence_id: competence_id
            }
            }).then(function (response) {
              if(response.data != '') {
                for(i in t.competences) {
                  if(t.competences[i].competence == competence_id) {
                    t.competences[i].lOptions = response.data;
                    //t.competences[i].level = t.competences[i].lOptions[0].id;
                  }
                }
              }
            })
            .catch(function (error) {
              console.log(error);
            });
          },
          submitForm: function() {
            var formData = new FormData();
            var t = this;
            if(this.id != '') {
              formData.append('id', this.id);
            }
            formData.append('name', this.name);
            formData.append('area', this.area);
            formData.append('competences', JSON.stringify(this.competences));

            axios.post('/companies/'+t.company+'/positions/post', 
              formData
            ).then(function(response) {
              if(response.data == 'ok') {
                window.location = '/companies/'+t.company+'/positions';
              }
            })
            .catch(function(error){
              console.log(error.response.data);
            });
          },
          loadPosition: function() {

          },
          editMode: function(id) {
            var t = this;
            axios.get('/position/json', {
            params: {
              id: id
            }
            }).then(function (response) {
              if(response.data != '') {
                console.log(response.data);
                t.id = response.data.id;
                t.name = response.data.name;
                t.area = response.data.company_area_id;
                t.dictionary = response.data.dictionary;
                t.competences = [];
                t.fetchedCompetences = response.data.competences;
                for(x in response.data.competences) {
                  t.competences.push({
                    competence : response.data.competences[x].id,
                    lOptions: [],
                    level: response.data.competences[x].pivot.competence_level_id
                  });
                  t.loadLevels(response.data.competences[x].id);
                  t.competences[x].level = response.data.competences[x].pivot.competence_level_id;
                }
              }
            })
            .catch(function (error) {
              console.log(error);
            });
          }
      }
   });


</script>

  <script>

    $.validate({
      form: '#position-form',
      addValidClassOnAll : true,
        lang: 'es',
        modules : 'date, security, location',
        onModulesLoaded : function() {
   
          // Show strength of password
          //$('input[name="password"]').displayPasswordStrength();
         }
    });

    $('#compentece-toggle').webuiPopover({
      title: "Competencias y niveles",
      width: '460',
      placement:'bottom'
    });

    var dd = $('.dd');
    dd.each(function(index, el) {
      var table = $(this);
      table.nestable({
        expandBtnHTML: '',
        collapseBtnHTML: '',
        callback: function(l,e) {
          data = table.nestable('toArray');
          console.log(data);
          axios.post('/positions/sort', {
            data: data
          }).then(function (response) {
            console.log(response);
          })
          .catch(function (error) {
            console.log(error);
          });
        }
      });
    });

    $('.open-edit-model').on('click', function(event) {
      event.preventDefault();
      var url = '/positions/' + $(this).data('id') + '/edit';
      $('#edit-model').find('form').attr('action', url);
      $('#edit-model').find('input.name').val($(this).data('name'));

      $('#edit-model').find('select.area').val($(this).data('area'));
      $('#edit-model').modal('toggle');
    });

    $('.open-delete-model').on('click', function(event) {
      event.preventDefault();
      var url = '/positions/' + $(this).data('id') + '/delete';
      $('#delete-model').find('form').attr('action', url);
      $('#delete-model').modal('toggle');
    });
  </script>
@endsection

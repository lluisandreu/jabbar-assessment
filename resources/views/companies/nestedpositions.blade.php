<ol class="dd-list">
    <li class="dd-list" data-id="{{ $child->id }}" data-type="position">
        <div class="dd-handle">{{ $child->name }}
          @foreach($child->competences as $comp)
            <div class="badge badge-primary"> 
              {{ $comp->name }} ({{ optional(\App\CompetenceLevel::find($comp->pivot->competence_level_id))->description }})
            </div> 
          @endforeach
        	<span class="float-right">
	            <a class="open-edit-model" data-id="{{ $child->id }}" data-name="{{ $child->name }}" data-area="{{ $child->company_area_id }}" data-competences="{{ $child->getCompetences() }}"><i class="fa fa-edit"></i></a>
	            <a class="open-delete-model" data-id="{{ $child->id }}"><i class="fa fa-trash"></i></a>
          </span>
        </div>
        @foreach($child->children as $child)
          @include('companies.nestedpositions', ['child' => $child])
        @endforeach
    </li>
</ol>
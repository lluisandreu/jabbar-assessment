@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form id="company-form" enctype="multipart/form-data" action="{{ url('companies') }}@isset($company)/{{ $company->id }}@endisset" method="POST" accept-charset="utf-8">
	@isset($company)
		{{ method_field('PUT') }}
		@endisset
	{{ csrf_field() }}
	<div class="row">
		<div class="col-md-12 col-lg-6">
			<div class="form-group ">
				<label class="label-control">Nombre de la empresa</label>
				<input type="text" class="form-control" placeholder="Nombre de la empresa" id="name" name="name" value="{{ old('name', isset($company->name)?$company->name:'') }}" data-validation="required">
			</div>
			<div class="form-group ">
				@isset($company->logo)
					<img src="{{  asset('storage/logos/' . $company->logo) }}"  alt="{{ $company->logo}}" class="logo" height="100">
				@endisset
				<label class="label-control">Logo</label>
				<input type="file" class="form-control" id="logo" name="logo">
			</div>
			<div class="form-group ">
				<label class="label-control">Sector</label>
				<select name="sector" class="form-control" data-validation="required">
					@foreach(App\CompanySector::all() as $sector)
						<option value="{{ $sector->id }}">{{ $sector->name }}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group ">
				<label class="label-control">Tamaño de la empresa</label>
				<select class="form-control" name="size" id="size" data-validation="required">
					@foreach(App\Company::sizes_array() as $key => $size)
						<option value="{{ $key }}" @isset($company->size) @if($company->size == $key) selected @endif @endisset @if(old('size') && old('size') == $key) selected @endif>{{ $size }}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group ">
				<button type="submit" class="btn btn-primary">@isset($company)Actualiza @else Crea @endisset</button>
			</div>
		</div>
	</div>
</form>

@section('inline-scripts')
	<script>
	$(document).ready(function() {
		$.validate({
			form: '#company-form',
			addValidClassOnAll : true,
		    lang: 'es',
		});
	});
	</script>
@endsection
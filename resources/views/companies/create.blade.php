@extends('layouts.assessment')
@push('styles')
  <link rel="stylesheet" type="text/css" href="/vendor/remark/global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.min.css">
@endpush
@section('content')
<div class="page animsition">
  <div class="page-header">
    @isset($company)
      <h1 class="page-title">Edita {{ $company->name }}</h1>
    @else 
      <h1 class="page-title">Crea una nueva empresa</h1>
    @endisset
  </div>
  <div class="page-content container-fluid">
    <div class="panel">
      <div class="panel-body">
        @include('companies.form')
      </div>
    </div>  
  </div>
</div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/locales/bootstrap-datepicker.es.min.js"></script>
@endsection
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

// Users
Route::resource('users', 'UserController');

// Profiles
Route::get('profiles/{id}/form', 'FormController@form')->name('profile_form');
Route::get('profiles/competences', 'ProfileController@competences_json');
Route::post('profiles/import', 'ProfileController@import')->name('profile_import');
Route::post('profiles/import/verify', 'ProfileController@verify')->name('profile_import_verify');
Route::post('profiles/import/store', 'ProfileController@store_import')->name('profile_import_store');
Route::get('profiles/import/success', function(Illuminate\Http\Request $request) {
	$request->session()->forget('rows');
	return redirect('profiles')->with('status', 'La importación ha sido un éxito');
});
Route::get('profiles/json', 'ProfileController@index_json')->name('profiles_json');
Route::resource('profiles', 'ProfileController');

// Companies & positions
Route::resource('companies', 'CompanyController');
Route::get('companies/{id}/positions', 'CompanyPositionController@index')->name('company_positions');
Route::post('companies/{id}/positions/post', 'CompanyPositionController@position_post')->name('new_position_post');
Route::post('positions/{id}/edit', 'CompanyPositionController@edit');
Route::post('positions/{id}/delete', 'CompanyPositionController@destroy');
Route::post('positions/sort', 'CompanyPositionController@position_sort');
Route::get('position/json', 'CompanyPositionController@position_json');
Route::get('positions/json', 'CompanyPositionController@index_json');

// Areas
Route::resource('areas', 'CompanyAreaController');

// Dictionaries
Route::resource('dictionaries', 'DictionaryController');
Route::get('dictionaries/index/json', 'DictionaryController@index_json');
Route::post('dictionaries/clone', 'DictionaryController@clone')->name('dictionary_clone');

// Competences
Route::resource('competences', 'DictionaryCompetenceController');
Route::get('dictionaries/{id}/competences', 'DictionaryCompetenceController@index')->name('dictionary_competences');
Route::get('dictionaries/{id}/competences/new', 'DictionaryCompetenceController@create')->name('dictionary_competences_create');
Route::post('dictionaries/{id}/competences/post/{cid?}', 'DictionaryCompetenceController@store')->name('dictionary_competences_post');;
Route::get('dictionaries/{id}/competences/{cid}', 'DictionaryCompetenceController@edit');
Route::get('dictionary/competences/json', 'DictionaryCompetenceController@competences_json');
Route::post('levels/{cid}/delete', 'DictionaryCompetenceController@level_delete');
Route::post('levels/{cid}/edit', 'DictionaryCompetenceController@level_edit');
Route::post('levels/sort', 'DictionaryCompetenceController@level_sort');
Route::get('levels/json', 'DictionaryCompetenceController@levels_json');
Route::post('competences/{id}/delete', 'DictionaryCompetenceController@destroy');
Route::post('competences/clone', 'DictionaryCompetenceController@clone')->name('competences_clone');

// Projects
Route::get('projects/json', 'ProjectController@index_json');
Route::resource('projects', 'ProjectController');

// Questionary
Route::resource('questionaries', 'QuestionaryController');
Route::post('questionaries/clone', 'QuestionaryController@clone')->name('questionary_clone');

// Evidences
Route::get('evidences/json', 'EvidenceController@index_json');
Route::get('questionaries/{id}/evidences/create', 'EvidenceController@create');
Route::get('questionaries/{did}/evidences/{id}/edit', 'EvidenceController@edit');
Route::resource('evidences', 'EvidenceController');
Route::get('evidenceindicators/json', 'EvidenceController@indicators_json');

// Forms
Route::get('submissions/json', 'SubmissionController@index_json');
Route::post('form/{id}/submission', 'FormController@post_submission');


Auth::routes();



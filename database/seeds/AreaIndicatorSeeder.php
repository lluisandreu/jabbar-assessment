<?php

use Illuminate\Database\Seeder;
use App\EvidenceArea as Area;

class AreaIndicatorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $knw = new Area();
        $knw->id = 1;
        $knw->name = 'Knowledge';
        $knw->save();

        $eng = new Area();
        $eng->id = 2;
        $eng->name = 'Engagement';
        $eng->save();

        $pot = new Area();
        $pot->id = 3;
        $pot->name = 'Potential';
        $pot->save();

        $knw->indicators()->createMany([
        	[
                'id' => 1,
        		'name' => 'Training',
        	],
        	[
                'id' => 2,
        		'name' => 'Background',
        	],
        	[  
                'id' => 3,
        		'name' => 'Business Skills',
        	]
        ]);

        $eng->indicators()->createMany([
        	[
                'id' => 4,
        		'name' => 'Company Engagement',
        	],
        	[
                'id' => 5,
        		'name' => 'Task Engagement',
        	],
        	[
                'id' => 6,
        		'name' => 'People Engagement',
        	]
        ]);

        $pot->indicators()->createMany([
        	[
                'id' => 7,
        		'name' => 'Mental Efficacy',
        	],
        	[
                'id' => 8,
        		'name' => 'Drivers',
        	],
        	[
                'id' => 9,
        		'name' => 'Emotional Skills',
        	]
        ]);

    }
}

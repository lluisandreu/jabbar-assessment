<?php

use Illuminate\Database\Seeder;

class CreateAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new \App\User();
        $admin->name = "admin";
        $admin->email = 'admin@admin.com';
        $admin->password = \Illuminate\Support\Facades\Hash::make('admin');
        $admin->status = 1;
		$admin->save();

		$role = \App\Role::where('name', 'admin')->first();

		$admin->roles()->attach($role);
    }
}

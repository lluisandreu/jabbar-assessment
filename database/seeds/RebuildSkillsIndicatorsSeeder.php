<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RebuildSkillsIndicatorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('skills')->truncate();
        DB::table('area_indicators')->truncate();
        DB::table('evidence_areas')->truncate();

        $this->call(SkillSeeder::class);
        $this->call(AreaIndicatorSeeder::class);
    }
}

<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class Provinces extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert("INSERT INTO `provinces` (`id`, `name`)
			VALUES
				(1, 'Álava'),
				(2, 'Albacete'),
				(3, 'Alicante'),
				(4, 'Almería'),
				(5, 'Asturias'),
				(6, 'Ávila'),
				(7, 'Badajoz'),
				(8, 'Barcelona'),
				(9, 'Burgos'),
				(10, 'Cáceres'),
				(11, 'Cádiz'),
				(12, 'Cantabria'),
				(13, 'Castellón'),
				(14, 'Ceuta'),
				(15, 'Ciudad Real'),
				(16, 'Córdoba'),
				(17, 'A Coruña'),
				(18, 'Cuenca'),
				(19, 'Girona'),
				(20, 'Granada'),
				(21, 'Guadalajara'),
				(22, 'Guipúzcoa'),
				(23, 'Huelva'),
				(24, 'Huesca'),
				(25, 'Illes Balears'),
				(26, 'Jaén'),
				(27, 'León'),
				(28, 'Lleida'),
				(29, 'Lugo'),
				(30, 'Madrid'),
				(31, 'Málaga'),
				(32, 'Melilla'),
				(33, 'Murcia'),
				(34, 'Navarra'),
				(35, 'Ourense'),
				(36, 'Palencia'),
				(37, 'Las Palmas'),
				(38, 'Pontevedra'),
				(39, 'La Rioja'),
				(40, 'Salamanca'),
				(41, 'Santa Cruz de Tenerife'),
				(42, 'Segovia'),
				(43, 'Sevilla'),
				(44, 'Soria'),
				(45, 'Tarragona'),
				(46, 'Teruel'),
				(47, 'Toledo'),
				(48, 'Valencia'),
				(49, 'Valladolid'),
				(50, 'Vizcaya'),
				(51, 'Zamora'),
				(52, 'Zaragoza'),
				(999, '(Sin especificar)');
			");
    }
}

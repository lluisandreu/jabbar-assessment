<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //$this->call(UsersTableSeeder::class);
        $this->call(Roles::class);
        $this->call(CreateAdminSeeder::class);
        $this->call(Provinces::class);
        $this->call(SkillSeeder::class);
        $this->call(AreaIndicatorSeeder::class);
        $this->call(CompanySectorSeeder::class);
        $this->call(RebuildSkillsIndicatorsSeeder::class);
    }
}

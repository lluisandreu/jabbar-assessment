<?php

use Illuminate\Database\Seeder;
use App\Skill;

class SkillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $business = new Skill();
        $business->id = 1;
        $business->name = "Business Skills";
        $business->indicator_id = 3;
        $business->save();

        $emotional = new Skill();
        $emotional->id = 2;
        $emotional->indicator_id = 9;
        $emotional->name = "Emotional Skills";
        $emotional->save();

        $analitical = new Skill();
        $analitical->id = 3;
        $analitical->indicator_id = 7;
        $analitical->name = "Analitical Skills";
        $analitical->save();
    }
}

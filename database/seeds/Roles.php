<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Role;
use App\Permission;

class Roles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $admin = new Role();
        $admin->id = 1;
		$admin->name         = 'admin';
		$admin->display_name = 'Admin'; // optional
		$admin->description  = 'Todos los permisos'; // optional
		$admin->save();

		$tech = new Role();
		$tech->id = 2;
		$tech->name         = 'project-manager';
		$tech->display_name = 'Jefe de Proyecto'; // optional
		$tech->description  = 'puede tocar todo pero solo para los proyectos que tenga asignados'; // optional
		$tech->save();

		$consu = new Role();
		$consu->id = 3;
		$consu->name         = 'consultor';
		$consu->display_name = 'Consultor'; // optional
		$consu->description  = 'Puede leer todos los perfiles de los proyectos en los que participe, pero solo puede editar los Perfiles que tenga asignados'; // optional
		$tech->save();

		$clie = new Role();
		$clie->id = 4;
		$clie->name         = 'client';
		$clie->display_name = 'Cliente'; // optional
		$clie->description  = 'Rellena los formularios'; // optional
		$clie->save();

		$users = new Permission();
		$users->id = 1;
		$users->name         = 'admin.users';
		$users->display_name = 'Administra usuarios'; // optional
		// Allow a user to...
		$users->description  = 'Administra usuarios'; // optional
		$users->save();

		$profiles = new Permission();
		$profiles->id = 2;
		$profiles->name         = 'admin.profiles';
		$profiles->display_name = 'Administra perfiles'; // optional
		// Allow a user to...
		$profiles->description  = 'Administra perfiles'; // optional
		$profiles->save();

		$company = new Permission();
		$company->id = 3;
		$company->name         = 'admin.companies';
		$company->display_name = 'Administra empresas'; // optional
		// Allow a user to...
		$company->description  = 'Administra empresas'; // optional
		$company->save();

		$project = new Permission();
		$project->id = 4;
		$project->name         = 'admin.projects';
		$project->display_name = 'Administra proyectos'; // optional
		// Allow a user to...
		$project->description  = 'Administra proyectos'; // optional
		$project->save();

		$project = new Permission();
		$project->id = 5;
		$project->name         = 'admin.projects';
		$project->display_name = 'Administra proyectos'; // optional
		// Allow a user to...
		$project->description  = 'Administra proyectos'; // optional
		$project->save();

		$admin->attachPermission(array($users, $profiles, $company, $project));
		$tech->attachPermission(array($users, $profiles, $company, $project));
    }
}

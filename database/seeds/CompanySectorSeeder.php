<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompanySectorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert("INSERT INTO `company_sectors` (`id`, `name`)
VALUES
	(1, 'Actividades informáticas'),
	(2, 'Actividades inmobiliarias'),
	(3, 'Actividades recreativas, culturales y deportivas'),
	(4, 'Administracion Pública y Defensa'),
	(5, 'Agricultura, ganadería y pesca'),
	(6, 'Alimentación y productos afines'),
	(7, 'Automoción'),
	(8, 'Comercio (al por mayor y al por menor)'),
	(9, 'Construcción'),
	(10, 'Correos y telecomunicaciones'),
	(11, 'Edición, artes gráficas y reproducción de soportes grabados'),
	(12, 'Educación'),
	(13, 'Electrónica y electricidad'),
	(14, 'Energía y minería'),
	(15, 'Industria del transporte'),
	(16, 'Industria ligera (mobiliario)'),
	(17, 'Industria madera/papel'),
	(18, 'Industria pesada (maquinaria)'),
	(19, 'Industria química y farmacéutica'),
	(20, 'Industria textil (textil/moda/hogar)'),
	(21, 'Investigación y Desarrollo'),
	(22, 'Sanidad'),
	(23, 'Servicios (hostelería, viajes, restauración)'),
	(24, 'Servicios de consultoría, asesoría y auditoría'),
	(25, 'Servicios de publicidad'),
	(26, 'Servicios de RRHH'),
	(27, 'Servicios de TI'),
	(28, 'Servicios financieros'),
	(29, 'Servicios personales'),
	(30, 'Otras actividades');");
    }
}

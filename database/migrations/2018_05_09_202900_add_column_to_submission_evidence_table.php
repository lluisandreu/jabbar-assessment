<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToSubmissionEvidenceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('submission_evidences', function (Blueprint $table) {
            $table->dropColumn('indicator_id');
        });

        Schema::table('submission_evidences', function (Blueprint $table) {
            $table->integer('type')->default(0);
            $table->integer('indicator_id')->nullable();
            $table->integer('competence_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

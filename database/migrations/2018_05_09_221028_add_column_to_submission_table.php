<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToSubmissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('submission_evidences');

        Schema::create('submission_values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('valuable_id');
            $table->string('valuable_type');
            $table->string('type');
            $table->integer('submission_id');
            $table->integer('indicator_id')->nullable();
            $table->integer('result');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

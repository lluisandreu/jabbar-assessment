<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('first_surname')->nullabel();
            $table->string('second_surname')->nullable();
            $table->date('birthdate')->nullabel();
            $table->string('telephone')->nullable();
            $table->string('email');
            $table->string('picture')->nullable();
            $table->string('province')->nullable();
            $table->string('city')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('position_id')->nullable();
            $table->string('function')->nullable();
            $table->date('evaluation_date')->nullable();
            $table->integer('author_id')->default(1);
            $table->integer('company_id')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}

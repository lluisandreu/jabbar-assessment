<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('profiles', function (Blueprint $table) {
            $table->integer('gender')->nullable()->after('status');
            $table->dropColumn('author_id');
            $table->integer('user_id')->after('gender')->unsigned();
        });

        Schema::table('profiles', function (Blueprint $table) {

            $table->foreign('user_id')
                ->references('id')->on('users');
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

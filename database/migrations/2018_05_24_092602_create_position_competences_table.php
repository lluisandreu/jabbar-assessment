<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePositionCompetencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('position_competences_pivot');

        Schema::create('position_competences', function (Blueprint $table) {
            $table->integer('company_position_id');
            $table->integer('dictionary_competence_id');
            $table->integer('competence_level_id');
            //$table->unique('company_position_id', 'dictionary_competence_id');

            // $table->foreign('company_position_id')
            //   ->references('id')->on('company_positions')
            //   ->onDelete('cascade');

            // $table->foreign('dictionary_competence_id')
            //   ->references('id')->on('dictionary_competences')
            //   ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('position_competences');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCascadeToTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();


        Schema::table('evidence', function (Blueprint $table) {

            $table->integer('questionary_id')->unsigned()->change();
        });

        Schema::table('evidence', function (Blueprint $table) {

            $table->foreign('questionary_id')
                ->references('id')->on('questionaries');
        });

        Schema::table('submissions', function (Blueprint $table) {

            $table->integer('profile_id')->unsigned()->change();
        });

        Schema::table('submissions', function (Blueprint $table) {

            $table->foreign('profile_id')
                ->references('id')->on('profiles');
        });

        Schema::table('submission_values', function (Blueprint $table) {

            $table->integer('submission_id')->unsigned()->change();
        });

        Schema::table('submission_values', function (Blueprint $table) {

            $table->foreign('submission_id')
                ->references('id')->on('submissions');
        });

        Schema::table('dictionary_competences', function (Blueprint $table) {

            $table->integer('dictionary_id')->unsigned()->change();
        });

        Schema::table('dictionary_competences', function (Blueprint $table) {

            $table->foreign('dictionary_id')
                ->references('id')->on('dictionaries');
        });

        Schema::table('projects', function (Blueprint $table) {

            $table->integer('company_id')->unsigned()->change();
        });

        Schema::table('projects', function (Blueprint $table) {

            $table->foreign('company_id')
                ->references('id')->on('companies');
        });

        Schema::table('company_positions', function (Blueprint $table) {

            $table->integer('company_id')->unsigned()->change();
        });

        Schema::table('company_positions', function (Blueprint $table) {

            $table->foreign('company_id')
                ->references('id')->on('companies');
        });

        Schema::table('competence_levels', function (Blueprint $table) {

            $table->integer('dictionary_competence_id')->unsigned()->change();
        });

        Schema::table('competence_levels', function (Blueprint $table) {

            $table->foreign('dictionary_competence_id')
                ->references('id')->on('dictionary_competences');
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

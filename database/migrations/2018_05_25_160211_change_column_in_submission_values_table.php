<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnInSubmissionValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('submission_values')->truncate();
        Schema::table('submission_values', function (Blueprint $table) {
            $table->dropColumn('result');
        });

        Schema::table('submission_values', function (Blueprint $table) {
            $table->string('result')->after('indicator_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
